$(function() {
    $(this).find("#mainBody").attr("data-name", 'Ken');
});

$(document).ready(function() {
    // --Bof JQuery to verify the OS type-->

    // Is iOs?
    var isIOS = /iPad|iPhone|iPod/i.test(navigator.userAgent);
    // Is Android?
	var isAndroid = /Android/i.test(navigator.userAgent);
	if (!isIOS && !isAndroid) {
		$("body").attr('data-window', 'NO mobile');
	} else if (isIOS) {
        $("body").attr('data-window', ' iOS');
        $("div#slider-area").each(function() {
            $(this).find(".slides").addClass('div-mobile');
        });
	} else if (isAndroid) {
        $("body").attr('data-window', 'Android');
        $("div#slider-area").each(function() {
            $(this).find(".slides").addClass('div-mobile');
        });
    }

    // --Bof JQuery for Preventing click activate-->
	$('a[href^="#"]').click(function(et) {
		et.preventDefault();

		return false;
	});

	$('.top_navigation li').hover(function(){
        $('ul', this).stop(true, true).slideDown(100);
        $(this).addClass("active");
    }, function(){
        $('ul', this).stop(true, true).slideUp(200);
        $(this).removeClass("active");
    });

    // --Bof JQuery to verify the width of window-->
    // Retrieve current document width
	var documentWidth = $(window).width();
	$("body").attr('data-started-width', documentWidth);

    // Function to call accodion menu
    // accordionMenus();

	$(window).on('resize', function() {
        // Retrieve current window width
		var windowWidth = $(window).width();
		$("body").attr('data-window-width', windowWidth);

		// //--Bof JQuery Option for sliding menu -->
		// if(windowWidth > 609 ) {
        //     $("body").removeAttr('style');
        //     $("body").removeAttr('class');
        //     $("#sidr-main").attr('style', "display:none;");
		// } else {
        //     if($("html").attr("style")){
        //         $("body").attr('style', "width: "+windowWidth+"px; position: absolute; right: 200px;");
        //         $("#sidr-main").attr('style', "display: block; right: 0px;");
        //     }
        // }

    }).trigger('resize');


    // --Bof JQuery for making smaller-->
    var scrolled_position = $(document).scrollTop().valueOf();
    if (scrolled_position >= 60) {
        $("#header").addClass('smaller');
    } else {
        $("#header").removeClass("smaller");
    }

    $(window).scroll(function() {
        // --For making header smaller-->
        var scroll = $(window).scrollTop();
        if (scroll >= 60) {
            $("#header").addClass('smaller');
            // $(".head_navigation").addClass('nodisplay');
        } else {
            $("#header").removeClass("smaller");
            // $(".head_navigation").removeClass('nodisplay');
        }

        // --For Back-to-top-->
        if ($(this).scrollTop() > 220) {
            $('.go-to-top').fadeIn(700);
        } else {
            $('.go-to-top').fadeOut(700);
        }
    });

    $('.go-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 700);

        return false;
    });
});


// --Bof for HTML5 placeholder fix-->
// $('[placeholder]').focus(function() {
//     var input = $(this);
//     if (input.val() == input.attr('placeholder')) {
//         input.val('');
//         input.removeClass('placeholder');
//     }
// }).blur(function() {
//     var input = $(this);
//     if (input.val() == '' || input.val() == input.attr('placeholder')) {
//         input.addClass('placeholder');
//         input.val(input.attr('placeholder'));
//     }
// }).blur().parents('form').submit(function() {
//     $(this).find('[placeholder]').each(function() {
//         var input = $(this);
//         if (input.val() == input.attr('placeholder')) {
//                 input.val('');
//         }
//     })
// });
// --Eof for HTML5 placeholder fix-->
