$(document).ready(function() {
    $('#form-signin').submit(function(event) {
        event.preventDefault(); 
        let email = $("input#loginEmail").val();
        let password = $("input#loginPassword").val();
        
        $this = $("#loginBtn");
        $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages

        $.ajax({
          url: "/login-process",
          type: "POST",
          data: {
            email: email,
            password: password
          },
          cache: false,
          success: function(data) {
            console.log(data); 
            let newData = JSON.parse(data);
            if (newData['code'] == 200) {
               
                // // remove all of tokens
                // localStorage.removeItem('authToken');
                // localStorage.removeItem('authId');
                // // assign new token
                // localStorage.setItem('authToken', newData['userToken']);
                // localStorage.setItem('authId', newData['userId']);

                // //clear all fields
                // $('#form-signin').trigger("reset");
    
                // // Success than redirect
                // $(location).attr('href', '/userinfo/' + newData['userId']);
            } else {
                // Fail message
                $('#loginMsg').html("<div class='alert alert-danger loginInfo'>");
                $('#loginMsg > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
                $('#loginMsg > .alert-danger').append($("<strong>").text(newData['message']));
                $('#loginMsg > .alert-danger').append('</div>');
                //clear all fields
                $('#form-signin').trigger("reset");
            }
          },
          complete: function() {
            setTimeout(function() {
                // Re-enable submit button when AJAX call is complete
                $this.prop("disabled", false); 
            }, 1000);

            setTimeout(function() {
                $( ".loginInfo" ).remove();
            }, 5000);
          }
        });
    });

    $("#loginEmail").focusout(function() {
        let button = $("#loginBtn");
        let email = $("input#loginEmail").val();

        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if (email.length != 0) {
            if (regex.test(email.toLowerCase())) {
                $( ".msg" ).remove();
                button.prop("disabled", false);
            } else {
                button.prop("disabled", true);
    
                $('#loginMsg').html("<div class='alert alert-danger msg'>");
                $('#loginMsg > .msg').append("Please use proper email address");
                $('#loginMsg > .msg').append('</div>');
            }
        }
    });

    // Pass data to modal
    $('#logOutModal').on('show.bs.modal', function (event) {
        // console.log($.cookie('PHPSESSID'));
        var myVal = 'Ken, ';
        $(this).find("#modalName").text(myVal);
    });

    $('#confirmedLogOut').click(function(event) {
        event.preventDefault(); 
        
        console.log('logOut');

        $("#logOutModal").modal('hide');
        $.ajax({
            url: "/logout",
            type: "GET",
            cache: false,
            success: function() {

            }
        });
    });    
});
