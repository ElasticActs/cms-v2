<?php
/** -------------------------------------------------------------------------
 * @package  ElasticActs CMS
 * @author   Kenwoo - iweb@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 *
 * Codign Style Guide http://www.php-fig.org/psr/psr-2/
 * Setting flag for parent file
 * ------------------------------------------------------------------------ */
define('_VALID_TAGS', 1);

$path = preg_replace('/\W\w+\s*(\W*)$/', '$1', realpath(dirname(__FILE__)));
define("CONFIG_ROOT", $path . '/');

require_once CONFIG_ROOT . 'vendor/autoload.php';

// Initialize basic values
$initial = new redBerg\Libs\initials\getInitial();

// Get userIP to check accessibility
$access = new redBerg\Libs\https\getAccess();
// Checking an end user access device
$access->getIsMobile();

// Routing set
$routing = new \redBerg\Libs\https\webRouter();
// Routing set
$routing->setWebRoute();

$initial->flushBuff();
