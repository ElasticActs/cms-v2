<?php
	$db = new \redBerg\App\models\database\getDB();

    $prefixURL = 'http://';
    if (CONFIG_ENABLE_SSL && $_ENV['HTTPS'] == 'on') {  
        $prefixURL = 'https://';
    }
	
	$type = 'topmenu';
	$menu = array();
	$query = "SELECT * FROM menu
				WHERE menutype = :type AND parent = :parent AND publish = '1' AND status = '1' AND hideshow = '1'
				ORDER BY ordering";
	$this->db->getQuery($query);
	$mainChannel = $this->db->getExecuteFetchAll(array(':type' => $type, ':parent' => 0));
	foreach ($mainChannel as $value) {
		$subMenu = array();
		$this->db->getQuery($query);
		$subChannel = $this->db->getExecuteFetchAll(array(':type' => $type, ':parent' => $value['id']));
		if (count($subChannel) > 0 ) {
			foreach ($subChannel as $subValue) {
				$subMenu[] = [
					'title' => $subValue['mtitle'],
					'shortDescription' => $subValue['short_description'],
					'subClass' => $subValue['subclass'],
					'link' => empty($subValue['link']) ? $subValue['alias'] : $subValue['link'],
					'targetWindow' => $subValue['target_window'],
				];
			}
		}

		$menu[] = [
			'title' => $value['mtitle'],
			'shortDescription' => $value['short_description'],
			'subClass' => $value['subclass'],
			'link' => empty($value['link']) ? $value['alias'] : $value['link'],
			'targetWindow' => $value['target_window'],
			'subMenu' => $subMenu
		];
	}

	return $menu;
?>