<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-light">

        <i class="fab fa-facebook-square"></i>

        <i class="fab fa-youtube"></i>
        <br/>

        Copyright © <?php echo date('Y'); ?> - redBergs. All Rights Reserved.
        </span>
    </div>
</footer>
