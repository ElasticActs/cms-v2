<?php
    $db = new \redBerg\App\models\database\getDB();

    $prefixURL = 'http://';
    if (CONFIG_ENABLE_SSL && $_ENV['HTTPS'] == 'on') {  
        $prefixURL = 'https://';
    }

    if (!empty($_SERVER['REQUEST_URI']) && isset($_SERVER['REQUEST_URI'])) {
        if (strpos(getenv('REQUEST_URI'), '?') > 0) {
            $uriValue = strstr(getenv('REQUEST_URI'), '?', true);
        } else {
            $uriValue = getenv('REQUEST_URI');
        }
    } else {
        $uriValue = getenv('REDIRECT_CI_PATH');
    }

    $basic_action = array("post","edit","delete","reply","register","activation","login","logout");

    $_ci_path = preg_replace("/\/$/", "", $uriValue);
    $_ci_path = preg_replace("/\&$/", "", $_ci_path);

    $pageInfo_array	= explode("/", $_ci_path);
    if ( in_array(end($pageInfo_array), $basic_action)) {
        $_ci_path = rtrim(str_replace(end($pageInfo_array), "", $_ci_path), "/");
    }

    $ci_array = explode("&" ,$_ci_path);
    if (count($ci_array) > 1) {
        $_ci_path = str_replace("&".end($ci_array), '', $_ci_path);
    }

    $ci_array = explode("/", $_ci_path);
    if (is_numeric(end($ci_array))) {
        $_ci_path = rtrim(str_replace(end($ci_array), "", $_ci_path), "/");
    }

    $_parent_ci_path = count($ci_array) > 2 ? "/". $ci_array[1] : $_ci_path;

    // Select Parent Id
    $mtQuery = "SELECT id, alias, link FROM menu WHERE alias = :ci_path OR link = :path_link AND parent = '0' AND publish = '1' AND status = '1' AND hideshow = '1'
				ORDER BY ordering";
    $db->getQuery($mtQuery);
	$parentElement = $db->getExecuteFetch(array(':ci_path' => $_parent_ci_path, ':path_link' => $_parent_ci_path));

    // Get all of sub menu based on Parent Id
	$query = "SELECT * FROM menu
				WHERE parent = :parent AND publish = '1' AND status = '1' AND hideshow = '1'
				ORDER BY ordering";
	$db->getQuery($query);
	$sideMenu = $db->getExecuteFetchAll(array(':parent' => $parentElement['id']));

    echo "<ul id=\"leftMenus\" class=\"leftMenus collapsible expandfirst\">\n";

    foreach ($sideMenu as $key => $value) {
        if(!empty($value['link'])) {
            $phrase_url = explode("//", $value['link']);
            $pLink = $phrase_url[0] == "http:" ? $value['link'] : $prefixURL . CONFIG_SITE_URL . $value['link'];
        } else {
            $pLink = $prefixURL . CONFIG_SITE_URL . $value['alias'];
        }

        $addClass = $_ci_path == $value['alias'] ? ' class="crt"' : '';

        echo "<li><a href='{$pLink}' title='{$value['mtitle']}' target='{$value['target_window']}'{$addClass}>{$value['mtitle']}</a>";

        // Get all child of sub menu based on sub Id
        $query = "SELECT * FROM menu
                    WHERE parent = :parent AND publish = '1' AND status = '1' AND hideshow = '1'
                    ORDER BY ordering";
        $db->getQuery($query);
        $subSideMenu = $db->getExecuteFetchAll(array(':parent' => $value['id']));
        if (count($subSideMenu) > 0) {
            $toOpen = $_ci_path == $value['alias'] ? ' class="now"' : '';

            if( empty($toOpen) ){
                $toOpen = $parentElement['alias'] == $value['alias'] ? ' class="now"' : '';
            }

            echo "\n<ul{$toOpen}>\n";

            foreach ($subSideMenu as $subKey => $subValue) {
                if(!empty($subValue['link'])) {
                    $subPhraseUrl = explode("//", $subValue['link']);
                    $subLink = $subPhraseUrl[0] == "http:" ? $subValue['link'] : CONFIG_SITE_URL.$subValue['link'];
                } else {
                    $subLink = CONFIG_SITE_URL.$subValue['alias'];
                }

                $subAddClass = $_ci_path == $subValue['alias'] ? ' class="crt ' : '';
                $childClass = empty($subAddClass) ?  ' class="children"' : $subAddClass.' children"';

                echo "<li><a href='{$subLink}' title='{$subValue['mtitle']}' target='{$subValue['target_window']}'{$childClass}>".$subValue['mtitle']."</a></li>\n";
            }

            echo "</ul>\n</li>\n";
        } else {
            echo "</li>\n";
        }
    }

    echo "</ul>\n";
?>
