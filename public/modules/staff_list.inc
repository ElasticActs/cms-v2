<ul class="staff_section">
<?php
    $db = new \redBerg\App\models\database\getDB();

    $prefixURL = 'http://';
    if (CONFIG_ENABLE_SSL && $_ENV['HTTPS'] == 'on') {  
        $prefixURL = 'https://';
    }
	
	$base_url = $prefixURL . CONFIG_SITE_URL;

	$mQuery = "SELECT * FROM staff WHERE publish = '1' AND status = '1' ORDER BY ordering";
    $db->getQuery($mQuery);
	$listItem = $db->getExecuteFetchAll(array());

	foreach ($listItem as $key => $value) {
		echo "<li><img src='".$base_url."/upload/".$value['filename']."' alt='".$value['name']."' width='150'><strong class=\"korea-txt\">".$value['name']."</strong>&nbsp;&nbsp;|&nbsp;&nbsp;".$value['jobtitle']."<br/><a href='mailto:".$value['email']."' title='Email to ".$value['name']."' target='_blank'>".$value['email']."</a>".$value['fulltxt']."</li>\n";
	}
?>
</ul>