<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

/**
 * Get Front Access method
 */
class getAccessPoint
{
    // use ElasticActs\App\models\baseModel;
    use \redBerg\Libs\libraries\getHandleElements;

    private $uriValue;
    private $globalValue;
    private $accessValue;
    private $http;
    private $Bearer;
    private $justURI;
    private $onlyURI;
    private $requestUri;
    private $allHeaders;
    private $headers;
    private $sanitize;
    private $extension;
    private $requestHeaders;

    private $db;

    // , 'search', 'register', 'activation', 'forgotPassword', 'resetPassword', 'login', 'logout'
    const basicAction = array('post', 'update', 'edit', 'delete', 'reply');
    const specialAction = array('download', 'api', 'xml', 'json', 'minifyHTML', 'minifyCSS', 'minifyJS');
    const xList = array("username", "password");

    /**
     * getAccessPoint constructor.
     */
    protected function __construct()
    {
        $this->http = array();
        $this->Bearer = array();
        $this->justURI = array();
        $this->onlyURI = array();
        $this->globalValue = array();
        $this->accessValue = array();
        
        $this->sanitize = new \redBerg\Libs\libraries\getSanitize();
        $this->db = new \redBerg\App\models\baseModel();

        // Parse needed information from PATH_INFO or REQUEST_URI
        if (!self::isEmpty($_SERVER['PATH_INFO']) && isset($_SERVER['PATH_INFO'])) {
            $this->uriValue = $_SERVER['PATH_INFO'];
        } else {
            if (!self::isEmpty($_SERVER['REQUEST_URI']) && isset($_SERVER['REQUEST_URI'])) {
                if (strpos($_SERVER['REQUEST_URI'], '?') > 0) {
                    $this->uriValue = strstr($_SERVER['REQUEST_URI'], '?', true);
                } else {
                    $this->uriValue = $_SERVER['REQUEST_URI'];
                }
            } else {
                $this->uriValue = $_SERVER['REDIRECT_CI_PATH'];
            }
        }

        $this->allHeaders = null;
    }

    /**
     * Get URI or CI path in array with actions
     *
     * @return array
     */
    protected function accessPoint()
    {
        if ($this->uriValue == '/') {
            $this->justURI =  array(
                'uri'      => '/',
                'langCode' => 'en',
                'langId'   => 1
            );
        } else {
            $this->justURI = $this->getJustURI($this->uriValue);
        }

        $this->accessValue['method'] = $_SERVER['REQUEST_METHOD'];
        $this->accessValue['uri'] = $this->sanitize->getSanitizing($this->justURI['uri'], 'uri');

        if (!self::isEmpty($this->justURI['langCode'])) {
            $this->accessValue['langCode'] = $this->justURI['langCode'];
        }

        if (!self::isEmpty($this->justURI['langId'])) {
            $this->accessValue['langId'] = $this->justURI['langId'];
        }

        if (!self::isEmpty($this->justURI['item_no'])) {
            $this->accessValue['ino'] = $this->justURI['item_no'];
        }

        if (!self::isEmpty($this->justURI['page_no'])) {
            $this->accessValue['pno'] = $this->justURI['page_no'];
        }

        if (!self::isEmpty($this->justURI['basic_act'])) {
            $this->accessValue['do'] = $this->justURI['basic_act'];
        }

        if (!self::isEmpty($this->justURI['special_act'])) {
            $this->accessValue['action'] = $this->justURI['special_act'];
        }

        if (!self::isEmpty(array_filter($this->setGlobalValue()))) {
            foreach ($this->setGlobalValue() as $key => $value) {
                if ($key != 'token') {
                    $this->accessValue['globalValue'] = $this->setGlobalValue();
                }
            }
        }

        // Get any token from header
        $headerSet = getallheaders();
        if (isset($headerSet['token'])) {
            $this->accessValue['token'] = $headerSet['token'];
        }

        // Get Bearer Token
        $this->Bearer = $this->getBearerToken();
        $this->accessValue['authToken'] = $this->Bearer;

        // Set content type in header
        new getHttps($this->accessValue);

        return $this->accessValue;
    }

    /**
     * Do not allow to use global $_POST and $_GET
     * Sanitizing values and convert to globalValue
     *
     * @return array
     */
    private function setGlobalValue()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                if (!self::isEmpty($_POST)) {
                    foreach ($_POST as $key => $kw) {
                        $this->globalValue[strtolower($key)] = in_array(strtolower($key), self::xList) ? $this->sanitize->getSanitizing($kw) : $this->sanitize->getSanitizing($kw, "simple");
                    }
                    unset($_POST);
                }
                break;
            default:
            case 'GET':
                if (!self::isEmpty($_GET)) {
                    foreach ($_GET as $key => $kw) {
                        $this->globalValue[strtolower($key)] = $this->sanitize->getSanitizing($kw, 'uri');
                    }
                    unset($_GET);
                }
                break;
        }

        return $this->globalValue;
    }

    /**
     * example URL as below
     * /sermons/all-sermons&3 or
     * index.php?node=article&id=38&p=4&m=view or
     * /download/images/201903/2019_3_10_page_0001_1552238035.jpg
     *
     * @param string $uValue
     * @return array
     */
    private function getJustURI($uValue)
    {
        $this->requestUri = strtr($uValue, "\\", "/");
        // Remove  "/" from the front
        $this->requestUri = preg_replace('/^\/+/', '', $this->requestUri);
        // Remove  "/" from the tail
        $this->requestUri = preg_replace('/\/$/', '', $this->requestUri);

        // Get Page number and remove it from URI
        $uri_array = explode('&', $this->requestUri);
        if (count($uri_array) > 1) {
            $this->onlyURI['page_no'] = end($uri_array);
        }

        if (!self::isEmpty($this->onlyURI['page_no'])) {
            $this->requestUri = str_replace('&' . end($uri_array), '', $this->requestUri);
        }

        // Make URI to array
        $uriArray = explode("/", $this->requestUri);
        $allActions = array_merge(self::basicAction, self::specialAction);

        // Check language form $this->requestUri
        $this->onlyURI['langCode'] = 'en';
        $this->onlyURI['langId'] = 1;
        $languageInfo = $this->db->getLanguageInfo(reset($uriArray));

        if ($languageInfo) {
            $this->requestUri = preg_replace('/^\/+/', '', str_replace(reset($uriArray), '', $this->requestUri));
            $this->onlyURI['langCode'] = $languageInfo['code'];
            $this->onlyURI['langId'] = $languageInfo['id'];
        }

        // Checking any actions been contained
        if (array_intersect($uriArray, $allActions)) {
            // Checking special action in requestUri.
            // If so, set special action from the first of array and remove it from requestUri
            if (in_array(reset($uriArray), self::specialAction)) {
                $this->onlyURI['special_act'] = reset($uriArray);
                $this->onlyURI['uri'] = preg_replace('/^\/+/', '', str_replace(self::specialAction, '', $this->requestUri));
            }

            // Checking basic action in requestUri.
            // If so, set basic action from the end of array and remove it from requestUri
            if (in_array(end($uriArray), self::basicAction)) {
                $this->onlyURI['basic_act'] = end($uriArray);
                // If special action been set above, than remove it for requestUri
                if (!self::isEmpty($this->onlyURI['basic_act'])) {
                    $this->requestUri = preg_replace('/^\/+/', '', str_replace($this->onlyURI['special_act'], '', $this->requestUri));
                }
                $this->onlyURI['uri'] = preg_replace('/\/$/', '', str_replace(self::basicAction, '', $this->requestUri));
            }

        } else {
            $this->onlyURI['uri'] = $this->requestUri;
        }

        // Get Item number and remove it from URI
        $page_array = explode("/", $this->onlyURI['uri']);
        if (is_numeric(end($page_array))) {
            $this->onlyURI['item_no'] = end($page_array);
            $this->onlyURI['uri'] = str_replace('/' . end($page_array), '', $this->onlyURI['uri']);
        }

        return $this->onlyURI;
    }

    /**
     * Get header Authorization
     */
    private function getAuthorizationHeader()
    {
        if (isset($_SERVER['Authorization'])) {
            $this->allHeaders = trim($_SERVER["Authorization"]);
        }

        if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $this->allHeaders = trim($_SERVER["HTTP_AUTHORIZATION"]);
        }

        if (function_exists('apache_request_headers')) {
            $this->requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $this->requestHeaders = array_combine(array_map('ucwords', array_keys($this->requestHeaders)), array_values($this->requestHeaders));

            if (isset($this->requestHeaders['Authorization'])) {
                $this->allHeaders = trim($this->requestHeaders['Authorization']);
            }
        }

        return $this->allHeaders;
    }

    /**
     * get access token from header
     */
    private function getBearerToken()
    {
        $this->headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!self::isEmpty($this->headers)) {
            if (preg_match('/Bearer\s(\S+)/', $this->headers, $matches)) {
                return $matches[1];
            }
        }

        return null;
    }
}
