<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

/**
 * Get http header
 */
abstract class getHeader
{
    use \redBerg\Libs\configs\definedConfig;
    use \redBerg\Libs\libraries\getHandleElements;

    /**
     * @var
     */
    protected $tempType;

    protected $fileExtension;

    protected $action;
    protected $file;
    protected $fileInfo = array();
    protected $prohibited = array();
    /**
     * @param $data
     */
    protected function setHeader($data)
    {
        $this->action = $data['action'];
        $this->file = $data['uri'];

        // Get contents type
        $this->fileExtension = self::getFileExtension($this->file);

        $this->tempType = $this->contentType($this->fileExtension);
        if (!self::isEmpty($this->fileExtension) && ($this->action === 'api' || $this->action === 'json' || $this->action === 'xml')) {
            $this->tempType = $this->contentType($this->action);
        }

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate('D, d M Y H:i:s') . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0, max-age=0", false);
        header("Cache-Control: private", false); // required for certain browsers

        // For downloading a file
        if (!self::isEmpty($this->fileExtension) && $this->action === 'download') {
            $this->fileInfo = $this->checkFileStatus($this->file, $this->fileExtension);

            $this->setFileHeader($this->tempType);

            // if the extension is pdf, it will force to open
            if ($this->fileExtension == 'pdf') {
                header("Content-Disposition: inline; filename=" . $this->fileInfo['name']);
            } else {
                header("Content-Disposition: attachment; filename=" . $this->fileInfo['name']);
            }

            header("Content-Length: " . filesize($this->fileInfo['location']));
            readfile("" . $this->fileInfo['location'] . "");
            exit();
        }

        if (!self::isEmpty($this->fileExtension) && ($this->fileExtension === 'xml' || $this->fileExtension === 'json')) {
            $this->fileInfo = $this->checkFileStatus($this->file, $this->fileExtension);

            $this->setFileHeader($this->tempType);

            header("Content-Disposition: inline; filename=" . $this->fileInfo['name']);
            // header("Content-Length: " . filesize($fileInfo['location']));
            readfile("" . $this->fileInfo['location'] . "");
            exit();
        }

        $this->setExtraHeader($this->tempType);
    }

    /**
     * @param $type
     */
    protected function setFileHeader($type)
    {
        header("Content-Type: $type; charset='utf-8'");
        header("Content-Transfer-Encoding: binary");
        header("Content-Encoding: gzip");
        header("Pragma: public"); // required
    }

    /**
     * @param $type
     */
    protected function setExtraHeader($type)
    {
        header("Content-Type: $type; charset='utf-8'");
        header("Content-Encoding: gzip");
        header("Pragma: no-cache");
    }

    /**
     * @param $file
     * @param $fileExtension
     * @return array
     */
    protected function checkFileStatus($file, $fileExtension)
    {
        // check file exits or not
        if (!self::checkFileExist($file)) {
            echo "<script type='text/javascript'>window.alert( '" . CONFIG_NOTICE_EXIST_NO_FILE . "' ); window.history.go( -1 );</script>";
            exit();
        }

        // check prohibited file by extension
        $this->prohibited = self::prohibited();
        foreach ($this->prohibited['extension'] as $value) {
            if (!stristr($fileExtension, $value) === "false") {
                echo "<script type='text/javascript'>window.alert( '" . CONFIG_NOTICE_EXIST_NO_FILE . "' ); window.history.go( -1 );</script>";
                exit();
            }
        }

        return [
            'location' => CONFIG_UPLOAD_ROOT . $file,
            'name' => end(explode("/", trim($file)))
        ];
    }

    /**
     * @param $fileExtension
     * @return mixed
     */
    abstract protected function contentType($fileExtension);
}
