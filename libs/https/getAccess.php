<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

/**
 * Get Front Access method
 */
class getAccess
{
    use \redBerg\Libs\configs\definedConfig;
    use \redBerg\Libs\libraries\getHandleElements;

    private $prefixURL;
    private $siteURL;
    private $protectByIP;
    private $endUserIP;
    private $userIP;

    /**
     * Getting User IP ||  This function was from CodeIgniter
     */
    public function __construct()
    {
        $this->prefixURL = self::checkHttp();
        $this->siteURL = $this->prefixURL . CONFIG_SITE_URL;

        $this->protectByIP = CONFIG_PROTECT_BY_IP;

        $cip = (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != "") ? $_SERVER['HTTP_CLIENT_IP'] : false;
        $rip = (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != "") ? $_SERVER['REMOTE_ADDR'] : false;
        $fip = (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != "") ? $_SERVER['HTTP_X_FORWARDED_FOR'] : false;

        if ($cip && $rip) {
            $this->userIP = $cip;
        } elseif ($rip) {
            $this->userIP = $rip;
        } elseif ($cip) {
            $this->userIP = $cip;
        } elseif ($fip) {
            $this->userIP = $fip;
        }

        if (strstr($this->userIP, ',')) {
            $x = explode(',', $this->userIP);
            $this->userIP = end($x);
        }

        if (!preg_match("/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/", $this->userIP)) {
            $this->userIP = '0.0.0.0';
        }

        unset($cip);
        unset($rip);
        unset($fip);

        $this->endUserIP = $this->userIP;

        // IP protected access
        $this->getAccessByIP();
    }

    /**
     * Check IP address to allow access
     * @return boolean
     */
    public function getAccessByIP()
    {
        $idx = 0;
        $ip_count = 0;
        while ($idx < count(self::allowedIPs())) {
            if (preg_match("/" . self::allowedIPs()[$idx] . "/", $this->endUserIP)) {
                $ip_count++;
            }

            $idx++;
        }

        if ($this->protectByIP == true && $ip_count <= 0) {
            self::redirectURL($this->siteURL);
        }
    }

    /**
     * Getting tht browser is for mobile
     *
     * @return boolean
     */
    public function getIsMobile()
    {
        static $is_mobile;

        if (isset($is_mobile)) {
            return $is_mobile;
        }

        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            $is_mobile = false;
        } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false // many mobile devices ( all iPhone, iPad, etc. )
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false) {

            $is_mobile = true;
        } else {
            $is_mobile = false;
        }

        return $is_mobile;
    }
}
