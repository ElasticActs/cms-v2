<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

use redBerg\Libs\https\getAccessPoint;

/**
 * Get Front Access method
 */
class getRouteBase extends getAccessPoint
{
    use \redBerg\Libs\libraries\getHandleElements;

    protected $accessPoints;
    protected $accessValue;
    protected $apiPoint = array();

    /**
     * getRouteBase constructor.
     */
    protected function __construct()
    {
        parent::__construct();

        $this->accessValue = $this->accessPoint();
        $this->accessPoints = array();
    }

    /**
     * @return array
     */
    protected function frontAccessPoint()
    {
        return $this->accessValue;
    }

    /**
     * This is to check request method
     * @param $method
     * @return bool
     */
    protected function checkRequestMethod($method)
    {
        if ($method !== self::stringToLower($_SERVER['REQUEST_METHOD'])) {
            return false;
        }

        return true;
    }

    /**
     * This is to set
     * @param $url
     * @param $classMethod
     */
    public function get($url = null, $classMethod = null)
    {
        if (!self::isEmpty($url) && !self::isEmpty($classMethod)) {
            array_push($this->apiPoint, array('get', $url, $classMethod));
        }
    }

    /**
     * @param $url
     * @param $classMethod
     */
    public function post($url = null, $classMethod = null)
    {
        if (!self::isEmpty($url) && !self::isEmpty($classMethod)) {
            array_push($this->apiPoint, array('post', $url, $classMethod));
        }
    }

    /**
     * @param $url
     * @param $classMethod
     */
    public function put($url = null, $classMethod = null)
    {
        if (!self::isEmpty($url) && !self::isEmpty($classMethod)) {
            array_push($this->apiPoint, array('put', $url, $classMethod));
        }
    }

    /**
     * @param $url
     * @param $classMethod
     */
    public function delete($url = null, $classMethod = null)
    {
        if (!self::isEmpty($url) && !self::isEmpty($classMethod)) {
            array_push($this->apiPoint, array('delete', $url, $classMethod));
        }
    }
}
