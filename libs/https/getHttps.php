<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

/**
 * Get Front Access method
 */
class getHttps
{
    /**
     * getHttps constructor.
     * Get proper header
     * @param $data
     */
    public function __construct($data)
    {
        new getContentType($data);
    }
}
