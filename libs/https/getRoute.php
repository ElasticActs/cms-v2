<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

/**
 * Get Front Access method
 */
class getRoute extends getRouteBase
{
    use \redBerg\Libs\libraries\getHandleElements;
    
    /**
     * @var array
     */
    public $route;

    private $log;
    private $action;
    private $target_url;

    /**
     * getRoute constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->log = new \redBerg\Libs\libraries\getWriteLog();

        // Routing with treatment of header
        $this->route = $this->frontAccessPoint();
    }

    /**
     * getRouting
     * get URI, do, action, and globalValue
     *
     * Now $url and $classMethod are always empty
     * We will not remove $url and $classMethod
     */
    public function getRouting()
    {
        parent::__construct();

        try {
            // Action will be used to set header
            // Handle api, json, xml
            $apiElements = array_filter($this->apiPoint, array($this, "apiFilter"));
            if (count($apiElements) == 1) {
                // Set dynamic class and method
                $arrayKey = array_keys($apiElements)[0];
                $methodIndicators = explode('@', $apiElements[$arrayKey][2]);

                $className = 'redBerg\\App\\controllers\\'. strval($methodIndicators[0]);
                $methodName = strval($methodIndicators[1]);

                $json = new $className($this->route);
                $json->$methodName();

                return;
            }

            throw new \Exception('There is no controller');

        } catch (\Exception $exception) {
            header('HT /1.1 400 Bad Request - ' . $exception->getMessage());
        }

        // Handle regular URL routing
        switch ($this->route['uri']) {
            case '/':
                $home = new \redBerg\App\controllers\homeController($this->route);
                $home->getContent();
                break;
            default:
                // This is for admin area for content management system
                // 'ewayplus' will be used admin indicator.
                if (strpos($this->route['uri'], 'ewayplus') !== false) {
                    $backEnd = new \redBerg\App\controllers\adminController($this->route);
                    $backEnd->getContent();
                    break;
                }

                // This is for admin area for membership management system
                // 'memberplus' will be used admin indicator.
                if (strpos($this->route['uri'], 'memberplus') !== false) {
                    $mrmEnd = new \redBerg\App\controllers\memberController($this->route);
                    $mrmEnd->getContent();
                    break;
                }

                if (isset($this->route['ino']) && !self::isEmpty($this->route['ino'])) {
                    $article = new \redBerg\App\controllers\articleController($this->route);
                    $article->getArticle();
                    break;
                }

                $frontEnd = new \redBerg\App\controllers\pageController($this->route);
                $frontEnd->getContent();
        }
    }

    // If request method does not match, it will return nothing
    public function apiFilter($var)
    {
        $this->action =isset($this->route['action']) && !self::isEmpty($this->route['action']) ? '/'. $this->route['action'] : '';
        $this->target_url = $this->action .'/'. $this->route['uri'];

        if ($this->checkRequestMethod($var[0]) && $var[1] === $this->target_url) {
            return $var;
        }

        // Return nothing
        return;
    }
}
