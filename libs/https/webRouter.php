<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\https;

class webRouter
{
    public $route;
    public $view;
    public $member;

    public function __construct()
    {
        $this->route = new \redBerg\Libs\https\getRoute();  
    }

    public function setWebRoute()
    {
        /**
         * The end of url which is numeric string will be treated as item number
         * The number with '&' will be treated as page number
         * so,you do not need to set an item number or page number in routing set
         */
        // Regular routing        
        $this->route->get('/ewayplus/login', 'loginController@getLogin');
        $this->route->get('/memberplus/login', 'loginController@getLogin');
        $this->route->get('/login', 'loginController@getLogin');
        $this->route->get('/account-info', 'memberController@getMember');
        $this->route->get('/register', 'memberController@newMember');
        $this->route->post('/login-process', 'loginController@processLogin');
        $this->route->get('/resetPassword', 'loginController@getResetPassword'); 
        $this->route->get('/logout', 'loginController@getLogout'); 

        /**
         * Basic Action = array('post', 'update', 'edit', 'delete', 'reply');
         * You don't have to create route for basic actions to use like below
         * /ewayplus/article/post
         * /ewayplus/article/edit?id=123
         * /ewayplus/article/update?id=123
         * /ewayplus/article//delete?id=123
         */
        $this->route->get('/ewayplus', 'adminController@getContent');
        $this->route->get('/ewayplus/category', 'adminController@getContent');
        $this->route->get('/ewayplus/section', 'adminController@getContent');
        $this->route->get('/ewayplus/menu', 'adminController@getContent');
        $this->route->get('/ewayplus/page', 'adminController@getContent');
        $this->route->get('/ewayplus/article', 'adminController@getContent');

        // API routing
        $this->route->get('/api/sermons/aa', 'jsonController@getContent');
        $this->route->get('/api/sermons/bb', 'jsonController@getContent');
        $this->route->get('/api/v1/sermons/cc', 'jsonController@getContent');

        $this->route->post('/api/v1/sermons', 'jsonController@getContent');
        //$this->route->put();
        //$this->route->delete();

        // Regular web base URL
        $this->route->getRouting();
    }
}
