<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Mian core controller
 */
class getWriteLog
{
    protected $_log_path;
    protected $_date;
    protected $level;
    protected $filePath;
    protected $message;
    protected $fp;

    public function __construct()
    {
        $this->_log_path = CONFIG_LOG;
        $this->_date =& date('Y-m-d H:i:s');
        $this->message = '';
    }

    public function writeLog($level = 'error', $msg)
    {
        $this->level = strtoupper($level);

        $this->filePath = $this->_log_path .'log_'. date('Y_m_d') .'.log';
        $this->message .= PHP_EOL. $this->level .' '. (($this->level == 'INFO') ? '-' : '-') .' '. $this->_date .' --> '.PHP_EOL. $msg;

        // if no log folder exist, create one
        if (!is_dir($this->_log_path)) {
            @mkdir($this->_log_path, 0777);
        }

        @chmod($this->filePath, FILE_WRITE_MODE);

        $this->fp = @fopen($this->filePath, FOPEN_WRITE_CREATE);
        flock($this->fp, LOCK_EX);
        fwrite($this->fp, $this->message);
        flock($this->fp, LOCK_UN);
        fclose($this->fp);

        return true;
    }
}
