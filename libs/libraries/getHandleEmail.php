<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Class handle other Elements
 */
trait getHandleEmail
{
    use \redBerg\Libs\configs\definedConfig;
    use \redBerg\Libs\libraries\getHandleElements;

    /**
     * Check the value is null or not
     *
     * @param $value
     * @return bool
     */
    public static function getCheckEmail( $email, $publicEmail = false, $dnsrrEmail = false ){

        if ( $publicEmail ) {
            return self::isPublicEmail( $email );
        }
    
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        if ( !preg_match( "/^[^@]{1,64}@[^@]{1,255}$/", $email ) ) {
            return false;
        }
    
        // Split it into sections to make life easier
        $email_array = explode( "@", $email );
        $local_array = explode( ".", $email_array[0] );
        for ( $i = 0; $i < sizeof( $local_array ); $i++ ) {
            if ( !preg_match( "/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i] ) ) {
                return false;
            }
        }
    
        // Check if domain is IP. If not, it should be valid domain name
        if ( !preg_match( "/^\[?[0-9\.]+\]?$/", $email_array[1] ) ) {
            $domain_array = explode( ".", $email_array[1] );
    
            // Not enough parts to domain
            if ( sizeof( $domain_array ) < 2 ) {
                return false;
            }
    
            for ( $i = 0; $i < sizeof( $domain_array ); $i++ ) {
                if ( !preg_match( "/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i] ) ) {
                    return false;
                }
            }
        }
    
        if ( $dnsrrEmail ) {
            return checkDNSRR( $email );
        }
    
        return true;
    }

    public static function isPublicEmail( $email )
    {
        $freeEmailList = self::freeEmails();
        $email_com = explode( '@', $email );
        if ( in_array( $email_com[1], $freeEmailList ) ) {
            return false;
        } else {
            $email_com_name = explode( '.', $email_com[1] );
            if ( in_array( $email_com_name[0], $freeEmailList ) ) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static function actualEmail( $email )
    {
        $actualEmail = str_replace( "&Dagger;", ".", trim( $email ) );
        $actualEmail = str_replace( "&#64;", "@", trim( $actualEmail ) );
    
        return $actualEmail;
    }
    
    public static function checkDNSRR( $email )
    {
        list( $userName, $hostName ) = explode( "@", self::actualEmail( $email ) );

        if( !self::isEmpty( $hostName ) ) {
            if (checkdnsrr($hostName , "MX")){
                return true;
            } else {
                return false;
            }
        }
    
        return false;
    }
}
