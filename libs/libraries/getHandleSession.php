<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Trait
 */
trait getHandleSession
{
    private $sessionLife_admin; //30mins in second
    private $logInTime;
    private $sessionId;
    private $sessionLife;
    private $current_time;
    private $checkIPStatusForSession;

    public function initialize()
    {
        // Table 'core_opt' from DB
        // CONFIG_LIFETIME_ADMIN = 3600 (30 mins in second)
        $this->sessionLife_admin = 3600;

        // Table 'core_opt' from DB
        // CONFIG_CHECK_IP_STATUS_FOR_SESSION true or false
        $this->checkIPStatusForSession = true; 

        $this->current_time = time();
    }

    /**
     * Checking Session
     */
    public function checkSession($base_url = null)
    {
        $this->initialize();

        if (isset($_SESSION['session_id']) && !self::isEmpty($_SESSION['session_id'])) {
            $this->logInTime   = $_SESSION['session_time'];
            $this->sessionId   = $_SESSION['session_id'];
            $this->sessionLife = $this->current_time - $this->logInTime;

            if ( $this->sessionLife < $this->sessionLife_admin ) {
                
                // $this->current_session_id = md5( $members['id'] . $members['members_email'] . $members['members_type'] . $this->current_time );

                // $_SESSION['session_id']         = $current_session_id;
                // $_SESSION['session_time']   = $current_time;
            }
        }

 

            // if ( $Bon_db->getTotalNumber( "members_session", "session_id != ''" ) > 0 ){
            //     $t_time     = time() - $session_life_admin;
            //     $dquery = "DELETE FROM members_session WHERE time < '". $t_time ."'";
            //     $Bon_db->getQuery( $dquery );
            // }

            // if ( CONFIG_CHECK_IP_STATUS_FOR_SESSION === "true" ){
            //     if ( $_ipno != $_SESSION['ipno'] ){
            //         // $extre = $_SESSION['section'] === "b" ? " AND section = 'b'" : "" ;

            //         // $query = "DELETE FROM members_session WHERE session_id = '". $session_id ."'". $extre. " AND userid = '".( int )$_SESSION['session_user_id'] ."'";
            //         // $Bon_db->getQuery( $query );
            //         // session_destroy();
            //         // clearstatcache();

            //         // echo "<script type=\"text/javascript\"> alert( 'Your IP address has been changed. Please login again.' ); window.location.reload( '". $base_url ."' ); </script>\n";
            //         // exit();
            //     }
            // }

            // $members    = $Bon_db->getMemberInfo( ( int )$_SESSION['session_user_id'], "members_email = '". $_SESSION['session_username']  ."'" );

            // if ( $session_life < $session_life_admin ){
            //     #Calculation of past date
            //     $past = date( 'Y-m-d H:i:s', time() - 7 * 60 * 60 * 24 );
            //     if ( $session_id == md5( $members['id'] . $members['members_email'] . $members['members_type'] . $logintime ) ){

            //         if ( $Bon_db->getTotalNumber( "members_session", "session_id = '". $session_id ."'" ) == 0 ){
            //             session_start();
            //             unset($_SESSION);
            //             session_destroy();

            //             echo "<script type=\"text/javascript\">parent.location=\"". $base_url ."\"; </script>\n";
            //         }
            //         else {
            //             $current_time   = time();
            //             $current_session_id  = md5( $members['id'] . $members['members_email'] . $members['members_type'] . $current_time );
            //             $query = "UPDATE members_session SET time = ". $current_time . ", session_id = '". $current_session_id ."' WHERE session_id = '". $session_id ."'";
            //         }

            //         if( $Bon_db->getQuery( $query ) ){
            //             $_SESSION['session_id']         = $current_session_id;
            //             $_SESSION['session_time']   = $current_time;
            //         }
            //         else{
            //             $query = "DELETE FROM members_session WHERE session_id = '". $session_id ."'";
            //             $Bon_db->getQuery( $query );
            //             unset($_SESSION);
            //             session_destroy();
            //             echo "<script type=\"text/javascript\"> alert( 'Please try to login again.' ); window.location.reload( '". $base_url ."' ); </script>\n";
            //             exit();
            //         }
            //     } else {
            //         $query = "DELETE FROM members_session WHERE session_id = '". $session_id ."'";
            //         $Bon_db->getQuery( $query );
            //         unset($_SESSION);
            //         session_destroy();
            //         echo "<script type=\"text/javascript\"> alert( 'Your seesion ID was not assigned.' ); window.location.reload( '". $base_url ."' ); </script>\n";
            //         exit();
            //     }
            // } else {
            //     $query = "DELETE FROM members_session WHERE session_id = '". $session_id ."' AND userid = '".( int )$_SESSION['session_user_id'] ."'";
            //     $Bon_db->getQuery( $query );
            //     unset($_SESSION);
            //     session_destroy();
            //     clearstatcache();

            //     echo "<script type=\"text/javascript\"> alert( 'Your seesion has expired. Please reconnect.' ); window.location.reload( '". $base_url ."' ); </script>\n";
            //     exit();
            // }
    }
}
