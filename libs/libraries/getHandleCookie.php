<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Trait
 */
trait getHandleCookie
{
    use \redBerg\Libs\libraries\getHandleElements;

    private $cookieDomain;

    public function __construct()
    {
        $this->cookieDomain = CONFIG_SITE_URL;
    }

    /**
     * Create Cookie
     */
    public function set_cookie($cookieName, $value, $expire = null)
    {
        if (self::isEmpty($expire)) {
            // 86400 = 1 day
            $expire = 86400 * 30;
        }

        setcookie($cookieName, base64_encode( $value ), time() + $expire ,'/', $this->cookieDomain);
    }

    /**
     * get Cookie
     */
    public function get_cookie($cookieName)
    {
        return base64_decode($_COOKIE[$cookieName]);
    }

    /**
     * Delete Cookie
     */
    public function unset_cookie($cookieName, $expire = 3600)
    {
        setcookie($cookieName, "", time() - $expire, '/', $this->cookieDomain);
    }
}
