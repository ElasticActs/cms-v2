<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Sanitizing functions
 */
class getSanitize extends getSanitizeBase
{
    public $output;

    /**
     * Sanitization
     * @param  string $input [description]
     * @param  string $type  [description]
     * @return string        [description]
     */
    public function getSanitizing( $input, $type = "string" )
    {
        if (is_array($input)) {
            foreach ( $input as $var => $val ) {
              $this->output[$var] = $this->getSanitizing( $val );
            }
            unset( $input );
        } else {
            if ( $type === 'astxt' ){
                $input = $this->getHtml2txt( $input );
            } elseif ( $type === "simple" ) {
                $input = str_replace( "<p>&nbsp;</p>", "", trim( $input ) );
                $input = $this->getKeepHtml( $input );
            } elseif ( $type === "nojs" ) {
                $input = $this->getKeepHtml( $input );
                $input = $this->getCleanUpJS( $input );
            } elseif ( $type === "uri" ) {
                $input = $this->getCleanUpURL( $input );
            } else {
                $input = $this->getBasicCleanUp( $input );
            }
            $this->output = trim( $input );
        }
        return $this->output;
    }
}
