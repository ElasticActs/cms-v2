<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

class getRenders
{
    use \redBerg\Libs\configs\definedConfig;
    use \redBerg\Libs\libraries\getHandleElements;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var string
     */
    private $inside;

    private $prefixURL;
    private $siteURL;
    private $allowedExtension;
    private $statusLeftMenu;
    private $statusLeft;
    private $statusRightMenu;
    private $statusRight;

    private $statusTopPage;
    private $statusBottomPage;

    private $menuHtml;
    private $loginPortion;
    private $html;
    private $sub;

    private $class;

    private $title;

    private $displayImg;

    private $displayBanner;

    /**
     * getViews constructor.
     */
    public function __construct()
    {
        $this->allowedExtension = self::allowedExtension();
        $this->filePath         = CONFIG_UPLOAD_ROOT;
        $this->inside           = null;
        $this->prefixURL        = self::checkHttp();
        $this->siteURL          = $this->prefixURL . CONFIG_SITE_URL;
    }

    /**
     * @param string $type
     * @param array $data
     * @return string
     */
    public function renderMenu(string $type, array $data)
    {
        $this->menuHtml = '';
        $this->html = "<!--Beginning of {$type} //-->";
        

        foreach ($data as $item) {
            $link = '';
            $targetWindow = '';
            $subClass = '';
            $title = '';
            $subMenu = '';
            extract($item);
        
            $this->html .= "<li class='nav-item'><a aria-label='{$title}' href='{$link}' target='{$targetWindow}' class='nav-link {$subClass}'>{$title}</a>";
            $this->sub = "";
            if (!self::isEmpty($subMenu)) {
                $this->sub  = "<!--Beginning of sub menu //-->";
                $this->sub .= "<ul class='child-ul' style='display: none;'>";
                foreach ($subMenu as $value) {
                    $link = '';
                    $targetWindow = '';
                    $subClass = '';
                    $title = '';
                    $subMenu = '';
                    extract($value);

                    $this->sub .= "<li class='child-item'><a aria-label='{$title}' href='{$link}' target='{$targetWindow}' class='child-link {$subClass}'>{$title}</a></li>";
                }
                $this->sub .= "</ul>";
            } 

            $this->html .= $this->sub ."</li>";
        }
        
        if ($type == 'topmenu') {
            $this->loginPortion = '
            <li class="nav-item">
                <a aria-label="Login" href="/login" target="_self" class="nav-link ">Login</a>
                <!--Beginning of sub menu //-->
                <ul class="child-ul" style="display: none;">
                    <li class="child-item"><a aria-label="Login" href="/login" target="_self" class="child-link ">Login</a></li>
                    <li class="child-item"><a aria-label="Register" href="/register" target="_self" class="child-link ">Register</a></li>
                </ul>
            </li>';

            if (isset($_SESSION['session_id']) && !self::isEmpty(($_SESSION['session_id']))) {
                $this->loginPortion = '
                <li class="nav-item">
                    <a aria-label="Login" href="/account-info" target="_self" class="nav-link ">Account</a>
                    <!--Beginning of sub menu //-->
                    <ul class="child-ul" style="display: none;">
                        <li class="child-item"><a aria-label="Account Info" href="/account-info" target="_self" class="child-link ">Account Info</a></li>
                        <li class="child-item"><a type="button" data-toggle="modal" data-target="#logOutModal" class="child-link ">Logout</a></li>
                    </ul>
                </li>';
            }
            
            // fixed-top
            // navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow
            $this->menuHtml = '<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark" id="header">
                            <div class="container">
                              <a class="navbar-brand brand-logo" href="/" aria-label="redBergs">redBergs</a>
                              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                              </button>
                              <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto top_navigation">
                                    '. $this->html . $this->loginPortion. '                                    
                                </ul>
                              </div>
                            </div>
                          </nav>';
        } else {
            $this->menuHtml = '<ul class="'. $type .' ml-auto">'. $this->html .'</ul>';
        }

        return $this->menuHtml;
    }

    /**
     * @param array $info
     * @return string
     */
    public function renderBreadcrumb(array $info)
    {
        $this->inside = '<!--Beginning of Breadcrumb-->';
        foreach ($info as $value) {
            $this->class = '';
            if ($value['menuName'] === 'home') {
                $this->class = ' class="home"';
            }

            // If there is no next element in array then this is last iteration
            if( next( $info ) ) {
                $this->inside .= "<li{$this->class}><a aria-label='{$value['menuName']}' href='{$value['link']}' title='{$value['menuName']}' target='_self'>{$value['menuName']}</a></li>";
            } else {
                $this->inside .= "<li><span>{$value['menuName']}</span></li>";
            }
        }

        return "<ul class='breadcrumbList'>{$this->inside}</ul>";
    }

    /**
     * @param array $info
     * @return string
     */
    public function renderFullContents(array $data)
    {
        $this->statusLeftMenu   = null;
        $this->statusLeft       = null;
        $this->statusRightMenu  = null;
        $this->statusRight      = null;

        $this->statusTopPage    = null;
        $this->statusBottomPage = null;

        $this->inside = '<!--Beginning of Full Contents-->';

        if (isset($data['allBanners'])) {
            foreach ($data['allBanners'] as $key => $value) {
                if (isset($value) && !self::isEmpty($value)) {
                    if ($key == 'top_banner') {
                        $data['contents']['topBanner'] .= $value;
                    }

                    if ($key == 'bottom_banner') {
                        $data['contents']['bottomBanner'] .= $value;
                    }
                }
            }
        }

        foreach ($data['allModules'] as $value) {
            if (isset($value['left_menu']) && !self::isEmpty($value['left_menu'])) {
                $this->statusLeftMenu .= $value['left_menu'];
            }

            if (isset($value['left']) && !self::isEmpty($value['left'])) {
                $this->statusLeft .= $value['left'];
            }

            if (isset($value['right_menu']) && !self::isEmpty($value['right_menu'])) {
                $this->statusRightMenu .= $value['right_menu'];
            }

            if (isset($value['right']) && !self::isEmpty($value['right'])) {
                $this->statusRight .= $value['right'];
            }

            if (isset($value['toppage']) && !self::isEmpty($value['toppage'])) {
                $this->statusTopPage .= $value['toppage'];
            }

            if (isset($value['bottompage']) && !self::isEmpty($value['bottompage'])) {
                $this->statusBottomPage .= $value['bottompage'];
            }

            if (isset($value['css']) && !self::isEmpty($value['css'])) {
                $data['contents']['css'] .= $value['css'];
            }

            if (isset($value['head_tracking']) && !self::isEmpty($value['head_tracking'])) {
                $data['contents']['headTracking'] .= $value['head_tracking'];
            }

            if (isset($value['top_js']) && !self::isEmpty($value['top_js'])) {
                $data['contents']['topJs'] .= $value['top_js'];
            }

            if (isset($value['top']) && !self::isEmpty($value['top'])) {
                $data['contents']['top'] .= $value['top'];
            }

            if (isset($value['banner']) && !self::isEmpty($value['banner'])) {
                $data['contents']['banner'] .= $value['banner'];
            }

            if (isset($value['user1']) && !self::isEmpty($value['user1'])) {
                $data['contents']['user1'] .= $value['user1'];
            }

            if (isset($value['user2']) && !self::isEmpty($value['user2'])) {
                $data['contents']['user2'] .= $value['user2'];
            }

            if (isset($value['user3']) && !self::isEmpty($value['user3'])) {
                $data['contents']['user3'] .= $value['user3'];
            }

            if (isset($value['foot_tracking']) && !self::isEmpty($value['foot_tracking'])) {
                $data['contents']['footTracking'] .= $value['foot_tracking'];
            }

            if (isset($value['foot_js']) && !self::isEmpty($value['foot_js'])) {
                $data['contents']['footJs'] .= $value['foot_js'];
            }

            if (isset($value['footer']) && !self::isEmpty($value['footer'])) {
                $data['contents']['bottom'] .= $value['footer'];
            }
        }

        if (!self::isEmpty($data['contents']['css'])) {
            $data['contents']['css'] = '<style>'. $data['contents']['css'] .'</style>';
        }

        if (!self::isEmpty($data['contents']['topJs'])) {
            $data['contents']['topJs'] = '<script>'. $data['contents']['topJs'] .'</script>';
        }

        if (!self::isEmpty($data['contents']['headTracking'])) {
            $data['contents']['headTracking'] = '<script>'. $data['contents']['headTracking'] .'</script>';
        }

        if (!self::isEmpty($data['contents']['footJs'])) {
            $data['contents']['footJs'] = '<script>'. $data['contents']['footJs'] .'</script>';
        }

        if (!self::isEmpty($data['contents']['footTracking'])) {
            $data['contents']['footTracking'] = '<script>'. $data['contents']['footTracking'] .'</script>';
        }


        $data['contents']['title'] = $this->renderTitle($data['contents']);

        if (!self::isEmpty($this->statusLeftMenu) || !self::isEmpty($this->statusLeft) && (self::isEmpty($this->statusRightMenu) && self::isEmpty($this->statusRight))) {
            $breakLine = self::isEmpty($this->statusLeftMenu) ? '' : '<br>';

            $data['contents']['mainCss'] = 'col-9 col-md-9';
            $data['contents']['leftSide'] = '
                <div class="col-3 col-md-3">'
                    . $this->statusLeftMenu .
                    $breakLine
                    . $this->statusLeft .
                '</div>';
            $data['contents']['fulltxt'] = $this->statusTopPage . $data['contents']['fulltxt'] . $this->statusBottomPage;

        } elseif (!self::isEmpty($this->statusLeftMenu) || !self::isEmpty($this->statusLeft) && (!self::isEmpty($this->statusRightMenu) || !self::isEmpty($this->statusRight))) {
            $leftBreakLine = self::isEmpty($this->statusLeftMenu) ? '' : '<br>';
            $rightBreakLine = self::isEmpty($this->statusRight) ? '' : '<br>';

            $data['contents']['mainCss'] = 'col-6 col-md-6';

            $data['contents']['leftSide'] = '
                <div class="col-3 col-md-3">'
                    . $this->statusLeftMenu .
                    $leftBreakLine
                    . $this->statusLeft .
                '</div>';
            $data['contents']['fulltxt'] = $this->statusTopPage . $data['contents']['fulltxt'] . $this->statusBottomPage;
            $data['contents']['rightSide'] = '
                <div class="col-3 col-md-3">'
                    . $this->statusRightMenu .
                    $rightBreakLine
                    . $this->statusRight .
                '</div>';
        } elseif (self::isEmpty($this->statusLeftMenu) && self::isEmpty($this->statusLeft) && (!self::isEmpty($this->statusRightMenu) || !self::isEmpty($this->statusRight))) {
            $breakLine = self::isEmpty($this->statusRightMenu) ? '' : '<br>';

            $data['contents']['mainCss'] = 'col-9 col-md-9';

            $data['contents']['fulltxt'] = $this->statusTopPage . $data['contents']['fulltxt'] . $this->statusBottomPage;
            $data['contents']['rightSide'] = '
                <div class="col-3 col-md-3">'
                    . $this->statusRightMenu .
                    $breakLine
                    . $this->statusRight .
                '</div>';
        } else {
            $data['contents']['mainCss'] = 'col-12 mx-auto';
            $data['contents']['fulltxt'] = $this->statusTopPage . $data['contents']['fulltxt'] . $this->statusBottomPage;
        }

        return $data['contents'];
    }

     /**
     * @param string $src
     * @param null $title
     * @param null $class
     * @return string
     */
    public function renderTitle(array $data)
    {
        $this->title = null;
        if (isset($data['title']) && !self::isEmpty($data['title'])) {
            if (isset($data['showtitle']) && $data['showtitle']) {
                if (isset($data['title_images']) && !self::isEmpty($data['title_images'])) {
                    $this->title = $this->renderImg($data['title_images'], $data['title']);
                } else {
                    $this->title = $data['title'];
                }
            }
        }

        return $this->title;
    }

    /**
     * @param string $src
     * @param null $title
     * @param null $class
     * @return string
     */
    public function renderImg($src, $title = null, $altText = null, $class = null, $maxWidth = null, $nailThumb = false, $useDiv = true)
    {
        if (!self::isEmpty($src)) {
            $fileArray = explode( ";", $src );
            if (count($fileArray) > 0) {
                foreach($fileArray as $key => $value) {
                    $imgLink = $this->siteURL ."/". CONFIG_UPLOAD_ROOT . $value;

					$extensionArray = explode( ".", $value);
                    $fileExtension  = end($extensionArray);

                    if (in_array($fileExtension, $this->allowedExtension['image'])) {
                        if (self::checkFileExist($src)) {
                            if (!self::isEmpty($title)) {
                                $title = self::isEmpty($altText) ? $title : $altText;

                                $this->title = 'alt="'. $title .'"';
                            }

                            if (!self::isEmpty($class)) {
                                $this->class = 'class="'. $class .'"';
                            }

                            list($width, $height)= @getimagesize($src);
                            if ( $width >= $maxWidth && 0 != $maxWidth ){
                                //$newWidth	= ( $nailThumb == true ) ? round( ( $width / $maxWidth ) * 100 ) : $maxWidth;
                                $newWidth = $maxWidth;
                            }
                            else {
                                $newWidth	= $width;
                            }

                            $newHeight = floor( ( $height / $width ) * $newWidth );

                            if ($nailThumb) {
                                $this->displayImg .= "<img src=\"". $imgLink ."\" alt=\"{$this->title}\"{$this->class} data-width=\"".$width."\"/>";
                            } else {
                                if ($useDiv) {
                                    $this->displayImg .= "<div{$this->class} style=\"margin:0 auto; max-width:{$newWidth}px;\"><img src=\"". $imgLink ."\" width=\"{$newWidth}\" height=\"{$newHeight}\" alt=\"{$this->title}\" data-width=\"".$width."\"/></div>";
                                } else {
                                    $this->displayImg .= "<img src=\"". $imgLink ."\" alt=\"{$this->title}\"{$this->class} data-width=\"".$width."\" style=\"margin:0 auto; max-width:{$newWidth}px;\"/>";
                                }
                            }

                        } else {

                        }

                    } else {

                    }
                }
            }
        } else {
            $src = self::isEmpty($src) ? 'No file' : $src;
            $this->displayImg = "<i class='text-danger'>{$src}</i> exist.";
        }

        return $this->displayImg;
    }

    public function renderBanner($src, $fullText = "", $altText = "", $targetUrl = "", $shtmlTitle = "", $targetWindow = "", $extraClass = "", $frontCheck, $asImg = false)
    {
        $banner = '';
        if (!self::isEmpty($src)) {
            $image_path	= $this->filePath . $src;
            $image_url = $this->siteURL ."/". CONFIG_UPLOAD_ROOT . $src;

            if (self::checkFileExist($src)) {
                list($width, $height) = @getimagesize($image_path);

                $imgs		= "<img src=\"{$image_url}\" alt=\"{$altText}\" width=\"$width\" height=\"$height\" data-width=\"$width\">";
                $extraClass	= !empty( $extraClass ) ? " class=\"". $extraClass ."\"" : "";

                //Front page banners
                if ($frontCheck == 1) {
                    $newFullText = !self::isEmpty($fullText) ? "<div class=\"container text-center\"><h3 class=\"banner-heading\">". $fullText ."</h3></div>" : "";

                    if (!self::isEmpty($targetUrl) ){
                        $this->displayBanner .= "<div class=\"slides\" style=\"background-image:url('". $image_url ."');\"><a href=\"". $targetUrl ."\" title=\"".$shtmlTitle."\" target=\"".$targetWindow."\">". $newFullText ."</a></div>";
                    } else {
                        $this->displayBanner .= "<div class=\"slides\" style=\"background-image:url('". $image_url ."');\">". $newFullText ."</div>";
                    }
                } else {
                    $newFullText = !self::isEmpty($fullText) ? "<div class=\"container text-center\"><h3 class=\"banner-heading\">". $fullText ."</h3></div>" : "";

                    if($asImg == true) {
                        $this->displayBanner .= "<div". $extraClass ." style=\"max-width: ".$width."px;\">". $imgs ."</div>\n";
                    } else{
                        $this->displayBanner .= "<div class=\"slides\" style=\"background-image:url('". $image_url ."');\">". $newFullText ."</div>";
                    }
                }
            } else {
                $this->displayBanner .= "";
            }
        } else {
            $this->displayBanner = "";
        }

        if (!self::isEmpty($this->displayBanner)) {
            $controlNaw = '';
            if ($frontCheck == 1) {
                $controlNaw = "<div id=\"slider-control-nav\"></div>";
            }

            $banner = "<!--Bof Front Slider-->\n<div class=\"slider-wrapper\">\n<div id=\"slider-area\">\n<!--Bof Actual Banner Sliding-->\n". $this->displayBanner ."\n<!--Bof Actual Banner Sliding-->\n</div>\n". $controlNaw ."</div>\n<!--Eof Front Slider-->";
        }

        return $banner;
    }

    /**
     * @param $embedCode
     * @param null $class
     * @return string
     */
    public function renderEmbed($embedCode, $class = null)
    {
        return "<div class='embed_video {$class}'>{$embedCode}</div>";
    }

    /**
     * @param string $files
     * @return array
     */
    public function renderFiles($files)
    {
        $fileArray = explode( ";", $files);

        $downloadableList = '<!---Beginning of downloadable file list-->';
        $displayableList = '<!---Beginning of displayable file list-->';
        $playableList = '<!---Beginning of playable file list-->';
        foreach ($fileArray as $value) {
            $justFile = explode('/', $value);
            $fileName = end($justFile);

                if (self::checkFileExist($value)) {
                    list( $width, $height )= getimagesize($this->filePath . $value);
                    if (in_array(self::getFileExtension($fileName), $this->allowedExtension['image'])) {
                        $displayableList .= "<p class='pageImg'><img src='/upload/{$value}' alt='{$fileName}' width='{$width}' height='{$height}'></p>";
                    }

                    if (in_array(self::getFileExtension($fileName), $this->allowedExtension['music'])) {
                        $playableList .= "<p class='mpTitle'><audio src='/upload/{$value}' preload='none'/></p>";
                    }
                }

                if (self::checkFileExist($value)) {
                    $locationToDownload = '/download/'. $value;
                    $downloadableList .= "<li><a aria-label='{$fileName}' href='{$locationToDownload}' title='{$fileName}' target='_self' onfocus='this.blur()'>{$fileName}</a></li>";
                } else {
                    $downloadableList .= "<li>{$fileName}</li>";
                }
        }

        return [
            'display' => $displayableList,
            'audio' => $playableList,
            'download' => "<ul class='downloadableFileList'>{$downloadableList}</ul>"
        ];
    }

        /**
     * @param string $outboundUrl
     * @return array
     */
    public function renderOutboundLink($outboundUrl)
    {
        $phraseUrl = explode(".", $outboundUrl);
        $second_phraseUrl = explode("/", $phraseUrl[0]);

        if (sizeof($phraseUrl) >= 2 && ($phraseUrl[0] == 'http://www' || $phraseUrl[0] == 'https://www')) {
            $link = $outboundUrl ;
        } elseif (sizeof($phraseUrl) >= 2 && ($second_phraseUrl[0] == 'http:' || $second_phraseUrl[0] == 'https:')) {
            $link = $outboundUrl ;
        } elseif (sizeof($phraseUrl) >= 2 && ($second_phraseUrl[0] != 'http:' || $second_phraseUrl[0] != 'https:')) {
            $link = "http://". $outboundUrl ;
        } elseif (sizeof($phraseUrl) == 3 && $phraseUrl[0] == 'www') {
            $link = "http://". $outboundUrl ;
        }

        return "<div class='outBoundLink'><a class='outboundLink' aria-label='{$link}' href='{$link}' title='{$link}' target='_blank' onfocus='this.blur()'>{$link}&nbsp;&nbsp;<i class='fas fa-external-link-alt'></i>
        </a></div>";
    }

    public function renderBoardList($type, $list, $pageNumber, $url)
    {
        $categoryType = ['casting', 'castingthumblist'];
        $this->inside = '<!--Beginning of Board List-->';

        switch ($type) {
            case ($type == 'castingthumblist'):
                $this->inside .= "<ul class='castingThumbList row'>";
                break;
            case ( $type == 'blog' ):
                $this->inside .= "<ul class=' row'>";
                break;
            default:
                $this->inside .= '<table class="table table-hover table-sm boardList"><tbody>';
                $this->inside .= '<tr id="board_bar" height="25">
                    <th scope="col" class="board_line lineTitle text-center">No.</th>
                    <th scope="col" class="board_line lineTitle text-center">Subject</th>
                    <th scope="col" class="board_line lineTitle text-center">File</th>
                    <th scope="col" class="board_line lineTitle text-center ">Date</th>
                    <th scope="col" class="lineTitle text-center d-none d-md-block">Views</th>
                </tr>';
                break;
        }

        $loopNumber	= $list['total_item'] - $pageNumber * CONFIG_NUMBER_OF_ARTICLES_PER_PAGE;
        foreach ($list['data'][$pageNumber] as $key => $value) {
            $bgClass = $key % 2 ? 'list_odd' : 'list_even';
            $displayDate = in_array($type, $categoryType) ? $value['casting_date'] : $value['publish_date'];
            $date = self::getShortFormatDate($displayDate);
            $link = $value['pagelink'].$value['external'];
            $attachment = $value['filename'] ? '+' : '-';

            switch ($type) {
                case ($type == 'castingthumblist'):
                    if ($loopNumber == $list['total_item']) {
                        $this->class = 'col-md-12';
                        $dtClass = 'col-md-6';
                        $ddClass = 'col-md-6';
                        $firstVideoClass = 'firstVideo';
                    } else {
                        $this->class = 'col-md-6';
                        $dtClass = 'col-md-4';
                        $ddClass = 'col-md-8';
                        $firstVideoClass = '';
                    }

                    if ($key < 5 && $pageNumber == 0) {
                        // This is for first 5 items.
                        $this->inside .= "<li class='list_{$key} {$firstVideoClass} odd {$this->class} pr-0 pl-0'>
                            <dl class='row'>
                                <dt class='{$dtClass} pr-0 pl-0'>
                                    <span class='vod_wrap-{$key}'>
                                    {$value['embed_bigcode']}
                                    </span>
                                </dt>
                                <dd class='{$ddClass} pr-0 pl-0'>
                                    <a href='{$link}' title='{$value['title']}' target='_self' onfocus='this.blur()'>
                                        <span class='vodTitle'>{$value['title']}</span>
                                        <span class='castingDate'>{$date}</span>
                                        {$value['casting_description']} / {$value['host']}
                                    </a>
                                </dd>
                            </dl>
                        </li>";
                    } else {
                        $topBorder = $key == 0 ? 'topBorder' : '';
                        $this->inside .= "<li class='list_{$key} {$topBorder} odd col-md-12 pr-0 pl-0 asList'>
                            <dl>
                                <dd class='col-md-12 pr-0 pl-0 mb-0'>
                                    <a href='{$link}' target='_self' onfocus='this.blur()' title=\"{$value['title']}\">
                                        <div class='row ml-0 mr-0'>
                                            <span class='col-lg-9 pl-0 pr-0 listVodTitle'><span class='listNumber'>{$loopNumber}</span>
                                            {$value['title']}
                                            </span>
                                            <span class='col-lg-3 pr-0 listCastingDate text-right'>
                                                <span class='bookName'>{$value['casting_description']}</span>
                                                <span class='speakerName'>{$value['host']}&nbsp;&nbsp;&nbsp;[{$date}]</span>
                                            </span>
                                        </div>
                                    </a>
                                </dd>
                            </dl>
                        </li>";
                    }
                    break;
                case ( $type == 'blog' ):
                    $this->inside .= "<ul class=' row'>";
                    break;
                default:
                    $indicator = $type == 'casting' ? $value['host'] : $attachment;
                    $this->inside .= "<tr height='25' class='{$bgClass}'>
                        <td scope='row' class='td_bot noLine td_font_size' align='center'>{$loopNumber}</td>
                        <td scope='col' class='td_bot padding_both' align='left'>
                            <a href='{$link}' target='_self' title='{$value['title']}' onfocus='this.blur()'>{$value['title']}</a>
                        </td>
                        <td scope='col' class='td_bot' align='center'>{$indicator}</td>
                        <td scope='col' class='td_bot_date' align='center'>{$date}</td>
                        <td scope='col' class='td_bot td_font_size d-none d-md-block' align='center'>{$value['views']}</td>
                    </tr>";
                    break;
            }

            $loopNumber--;
        }

        if ($type == 'castingthumblist') {
            $this->inside .= "</ul>";
        } elseif ($type == 'blog') {

        } else {
            $this->inside .= "</tbody></table>";
        }

        return "<div class='boardListContainer'>temp {$list['total_item']}-{$list['total_page']}<br/>{$this->inside}</div>{$this->renderPagination($list, $pageNumber + 1, $url)}";
    }

    public function renderList($type, $list, $pageNumber, $url)
    {
        $categoryType = ['casting', 'castingthumblist'];
        $this->inside = '<!--Beginning of Board List-->';

        switch ($type) {
            case ($type == 'castingthumblist'):
                $this->inside .= "<ul class='castingThumbList row'>";
                break;
            case ( $type == 'blog' ):
                $this->inside .= "<ul class=' row'>";
                break;
            default:
                $this->inside .= '<table class="table table-hover table-sm boardList"><tbody>';
                $this->inside .= '<tr id="board_bar" height="25">
                    <th scope="col" class="board_line lineTitle text-center">No.</th>
                    <th scope="col" class="board_line lineTitle text-center">Subject</th>
                    <th scope="col" class="board_line lineTitle text-center">File</th>
                    <th scope="col" class="board_line lineTitle text-center ">Date</th>
                    <th scope="col" class="lineTitle text-center d-none d-md-block">Views</th>
                </tr>';
                break;
        }

        $loopNumber	= $list['total_item'] - $pageNumber * CONFIG_NUMBER_OF_ARTICLES_PER_PAGE;
        foreach ($list['data'][$pageNumber] as $key => $value) {
            $bgClass = $key % 2 ? 'list_odd' : 'list_even';
            $displayDate = in_array($type, $categoryType) ? $value['casting_date'] : $value['publish_date'];
            $date = self::getShortFormatDate($displayDate);
            $link = $value['pagelink'].$value['external'];
            $attachment = $value['filename'] ? '+' : '-';

            switch ($type) {
                case ($type == 'castingthumblist'):
                    if ($loopNumber == $list['total_item']) {
                        $this->class = 'col-md-12';
                        $dtClass = 'col-md-6';
                        $ddClass = 'col-md-6';
                        $firstVideoClass = 'firstVideo';
                    } else {
                        $this->class = 'col-md-6';
                        $dtClass = 'col-md-4';
                        $ddClass = 'col-md-8';
                        $firstVideoClass = '';
                    }

                    if ($key < 5 && $pageNumber == 0) {
                        // This is for first 5 items.
                        $this->inside .= "<li class='list_{$key} {$firstVideoClass} odd {$this->class} pr-0 pl-0'>
                            <dl class='row'>
                                <dt class='{$dtClass} pr-0 pl-0'>
                                    <span class='vod_wrap-{$key}'>
                                    {$value['embed_bigcode']}
                                    </span>
                                </dt>
                                <dd class='{$ddClass} pr-0 pl-0'>
                                    <a href='{$link}' title='{$value['title']}' target='_self' onfocus='this.blur()'>
                                        <span class='vodTitle'>{$value['title']}</span>
                                        <span class='castingDate'>{$date}</span>
                                        {$value['casting_description']} / {$value['host']}
                                    </a>
                                </dd>
                            </dl>
                        </li>";
                    } else {
                        $topBorder = $key == 0 ? 'topBorder' : '';
                        $this->inside .= "<li class='list_{$key} {$topBorder} odd col-md-12 pr-0 pl-0 asList'>
                            <dl>
                                <dd class='col-md-12 pr-0 pl-0 mb-0'>
                                    <a href='{$link}' target='_self' onfocus='this.blur()' title=\"{$value['title']}\">
                                        <div class='row ml-0 mr-0'>
                                            <span class='col-lg-9 pl-0 pr-0 listVodTitle'><span class='listNumber'>{$loopNumber}</span>
                                            {$value['title']}
                                            </span>
                                            <span class='col-lg-3 pr-0 listCastingDate text-right'>
                                                <span class='bookName'>{$value['casting_description']}</span>
                                                <span class='speakerName'>{$value['host']}&nbsp;&nbsp;&nbsp;[{$date}]</span>
                                            </span>
                                        </div>
                                    </a>
                                </dd>
                            </dl>
                        </li>";
                    }
                    break;
                case ( $type == 'blog' ):
                    $this->inside .= "<ul class=' row'>";
                    break;
                default:
                    $indicator = $type == 'casting' ? $value['host'] : $attachment;
                    $this->inside .= "<tr height='25' class='{$bgClass}'>
                        <td scope='row' class='td_bot noLine td_font_size' align='center'>{$loopNumber}</td>
                        <td scope='col' class='td_bot padding_both' align='left'>
                            <a href='{$link}' target='_self' title='{$value['title']}' onfocus='this.blur()'>{$value['title']}</a>
                        </td>
                        <td scope='col' class='td_bot' align='center'>{$indicator}</td>
                        <td scope='col' class='td_bot_date' align='center'>{$date}</td>
                        <td scope='col' class='td_bot td_font_size d-none d-md-block' align='center'>{$value['views']}</td>
                    </tr>";
                    break;
            }

            $loopNumber--;
        }

        if ($type == 'castingthumblist') {
            $this->inside .= "</ul>";
        } elseif ($type == 'blog') {

        } else {
            $this->inside .= "</tbody></table>";
        }

        return "<div class='boardListContainer'>temp {$list['total_item']}-{$list['total_page']}<br/>{$this->inside}</div>{$this->renderPagination($list, $pageNumber + 1, $url)}";
    }

    public function renderPagination($list, $pageNumber, $url)
    {
        $pagination = "<div class='page_numbers_area'>";
        $totalBlock = ceil( $list['total_page']/CONFIG_NUMBER_OF_PAGES_PER_BLOCK );
        $block 		= ceil( $pageNumber/CONFIG_NUMBER_OF_PAGES_PER_BLOCK );
        $firstPage  = ( $block-1 )*CONFIG_NUMBER_OF_PAGES_PER_BLOCK;
        $lastPage 	= $block*CONFIG_NUMBER_OF_PAGES_PER_BLOCK;

        if ( $totalBlock <= $block ) {
            $lastPage = $list['total_page'];
        }

        //Link to preview page block
        if ( $block > 1 ){
            $myPage = $firstPage;
            $pagination .= " <a href='/{$url}&amp;{$myPage}' class='page_numbers' onfocus='this.blur()'><span>&laquo;</span></a> ";
        }

        //Link to the page directly
        for ( $directPage = $firstPage + 1; $directPage <= $lastPage; $directPage++ ) {
            $pagination .= $pageNumber == $directPage ? "<strong class='page_numbers current'>{$directPage}</strong> ": " <a href='/{$url}&amp;{$directPage}' onfocus='this.blur()' class='page_numbers'>{$directPage}</a> ";
        }

        //Link to next page block
        if ( $block < $totalBlock ) {
            $myPage = $lastPage+1;
            $pagination .= " <a href='/{$url}&amp;{$myPage}' class='page_numbers' onfocus='this.blur()'><span>&raquo;</span></a> ";
        }

        $pagination .= "</div>";

        return $pagination;
    }
}
