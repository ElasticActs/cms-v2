<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

class getEmail 
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $email;
    public $sendGrid;

    /**
     * getViews constructor.
     */
    public function __construct()
    {
        try {
            if (isset($_ENV['SENDGRID_API_KEY']) && !self::isEmpty($_ENV['SENDGRID_API_KEY'])) {
                $this->sendGrid = new \SendGrid($_ENV['SENDGRID_API_KEY']);
            } else {
                throw new \Exception('There is no API Key');
            }
        } catch (\Exception $exception) {
            header('HT /1.1 503 Service Unavailable - ' . $exception->getMessage());
            die();
        }
    }

    public function sendEmail(array $body)
    {
        $this->email = new \SendGrid\Mail\Mail(); 
        
        $this->email->setFrom("no-reply@kenwoo.ca", "Example User");
        $this->email->setSubject($body['subject']);
        $this->email->addTo($body['to'], $body['toName']);
        $this->email->addContent("text/html", $body['body']);

        try {
            $response = $this->sendGrid->send($this->email);
            if ($response->statusCode() != 200) {
                throw new \Exception($response->body());
            }
        } catch (\Exception $e) {
            echo 'Caught exception: '. $e->getMessage();
        }
    }
}
