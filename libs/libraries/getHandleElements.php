<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Class handle other Elements
 */
trait getHandleElements
{
    use \redBerg\Libs\configs\definedConfig;

    /**
     * Check the value is null or not
     *
     * @param $value
     * @return bool
     */
    public static function isEmpty($value)
    {
        if ( is_array($value) ) {
            if (sizeof($value) > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            if ( (is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL')  && (!is_null($value)) && (strlen(trim($value)) > 0) ) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Redirect to another page or site
     *
     * @param string $url
     */
    public static function redirectURL($url)
    {
        if (headers_sent()) {
            echo "<script>document.location.href='{$url}';</script>";
        } else {
            @ob_end_clean();    //Clear output buffer
            header('HTTP/1.1 301 Moved Permanently');
            header("Location: ". $url );
        }

        // Set a 400 (bad request) response code and exit.
        http_response_code(400);
        exit();
    }

    /**
     * To cut string by certain length
     *
     * @param string $str
     * @param int $start
     * @param $length
     * @return string|string[]|null
     */
    public static function mbStrCut(string $str, $start = 0, $length)
    {
        // Strip HTML and PHP tags from a string
        $string = trim(strip_tags($str));
        // Replace <p> with <br> tag
        $string = str_replace(array("<p>", "</p>"), array("<br>", "<br>"),  $string);
        // Remove first <br> tag
        $string = preg_replace('/^<br\s?\>/', '', $string);

        if (mb_strlen($string,'UTF-8 ') > $length){
             // Checking Korean with special
            $languageCheck = "/^[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}가-힣0-9\s]+$/";
            if (preg_match($languageCheck, $string)) {
                $length = 25;
            }

            /* mb_substr  PHP 4.0 이상, iconv_substr PHP 5.0 이상 */
            $cutString  = mb_substr($string, $start, $length,'UTF-8 ') . "..";
        } else {
            $cutString = $string;
        }

        return $cutString;
    }

    /**
     * Required Design files
     *
     * @param $design_directory
     * @return array
     */
    public static function allViews($design_directory)
    {
        $directory_array = array();
        if ($dir = @dir($design_directory)) {
            while ($file = $dir->read()) {
                if (!is_dir($design_directory . $file) && (!preg_match("/^[_]/", $file)) && ($file != "index.php" && $file != "." && $file != "..")) {
                    $directory_array[] = array('id' => $file, 'text' => self::stringToUpper($file));
                }
            }
            sort($directory_array);
            $dir->close();
        }

        return $directory_array;
    }

    /**
    * Make a string lowercase.
    *
    * @param string The string to convert to lowercase.
    * @return string The converted string.
    */
    public static function stringToLower(string $str)
    {
        if (function_exists("mb_strtolower")) {
            return mb_strtolower($str, "UTF-8");
        } else {
            return strtolower($str);
        }
    }

    /**
    * Make a string uppercase.
    *
    * @param string The string to convert to uppercase.
    * @return string The converted string.
    */
    public static function stringToUpper(string $str)
    {
        if (function_exists("mb_strtoupper")) {
            return mb_strtoupper($str, "UTF-8");
        } else {
            return strtoupper($str);
        }
    }

    /**
    * Get a file extension.
    *
    * @param string
    * @return string
    */
    public static function getFileExtension(string $str)
    {
        return strtolower(substr(strrchr($str, "."), 1));
    }

    /**
     * Check expired
     *
     * @param $startDate
     * @param $endDate
     * @return bool
     */
    public static function getBetweenDates($startDate, $endDate) {
        if (($startDate == 0 || $startDate > 0 && $startDate < time()) && ($endDate == 0 || $endDate > 0 && $endDate > time())) {
            return true;
        }

        return false;
    }

    /**
     * Check an array for a specified key
     *
     * @param $key
     * @param $array
     * @return bool
     */
    public static function checkKeyExist($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check a file exist or not
     *
     * @param $name
     * @return bool
     */
    public static function checkFileExist($name)
    {
        $filePath = preg_replace('/\W\w+\s*(\W*)$/', '$1', realpath(dirname(__DIR__))) .'/public/'. CONFIG_UPLOAD_ROOT . $name;
        if (file_exists($filePath)) {
            return true;
        }

        return false;
    }

    public static function getShortFormatDate($raw_date)
    {
        if ( $raw_date == '' ) {
            return false;
        }

        $year	= substr( $raw_date, 0, 4 );
        $month	= ( int )substr( $raw_date, 5, 2 );
        $day	= ( int )substr( $raw_date, 8, 2 );

        return date( "M.d.y", mktime( 0, 0, 0, $month, $day, $year ) );
    }

    public static function getLongFormatDate($raw_date)
    {
        if ( $raw_date == '' ) {
            return false;
        }

        $year	= ( int )substr( $raw_date, 0, 4 );
        $month	= ( int )substr( $raw_date, 5, 2 );
        $day 	= ( int )substr( $raw_date, 8, 2 );

        return date( "M d, Y", mktime( 0, 0, 0, $month, $day, $year ) );
    }

    public static function getProperLink($url)
    {
        $prefixURL = self::checkHttp();
        $siteURL = $prefixURL . CONFIG_SITE_URL;

        $phrase_url = explode( ".", $url );
		if ($phrase_url[0] != "www" && sizeof($phrase_url) == 1 ) {
           $link =  $siteURL . $url ."";
        } elseif ( $phrase_url[0] == "www" && sizeof($phrase_url) == 3 ) {
			$link = "http://". $url ."";   
		} elseif ($phrase_url[0] == "http://www") {
		    $link = $url;
		} elseif ($phrase_url[0] == "https://www") {
		    $link = $url;
        } elseif ($phrase_url[0] != "www") {
            $second_phrase_url = explode( "/", $phrase_url[0] );
			if (sizeof($phrase_url) == 3) {
                if ($second_phrase_url[0] == "https:") {
                    $link = $url;
                } else {
                    $link = "http://". $url ."";
                }
		    } else {
                if (end($second_phrase_url) != "www" && $second_phrase_url[0] == "http:") {
                    $link = $url;
                } else {
                    $link = "http://www.". $url ."";
                }
			}
		}

		return $link;
    }

    /**
     * Print out error
     *
     * @param $data
     */
    public static function errorOutput($data)
    {
        $output = array();
        $errorOutput = null;
        if (is_array($data) &&  count($data) > 0) {
            foreach ($data as $key => $value) {
                $va          = strtolower($key);
                $output[$va] = $value;
            }
            unset( $data );

            $i = 1;
            foreach ($output as $keys => $vals) {
                $extra_tail   = $i == count( $output ) ? '' : '<br/>';
                $errorOutput .= '<strong>'. self::stringToUpper($keys) .': </strong>'. $vals . $extra_tail;
                $i++;
            }

            $errorOutput = "<div style='line-height: 1.5em;'>". $errorOutput ."</div>";
        } else {
            $errorOutput = $data;
        }

        echo $errorOutput;
    }

    /**
     * Print out
     *
     * @param $data
     */
    public static function printOut($data)
    {
        if (is_array($data) &&  count($data) > 0) {
            $printOut = print_r($data,true);
        } elseif (is_bool($data)) {
            $printOut = var_dump($data);
        } else {
            $printOut = $data;
        }

        echo "<pre style='font-size:1em;line-height:1em;'>" . $printOut . "</pre>";
    }
    
    public static function deBug($values, $location = null)
    {
        $_date_fmt = 'Y-m-d H:i:s';
        $_date =& date($_date_fmt);

        if (!is_null($location)) {
            $location = $location .'<br>';
        }

        if (!is_null($values)) {
            $typeString = '';
            if (is_array($values)) {
                $pieces = $values;
                $typeString = 'Array';
            } elseif (is_object($values)) {
                $pieces = json_decode(json_encode($values, true), true);
                $typeString = 'Object';
            } else {
                $pieces = explode(',', $values);
                $typeString = 'String';
            }

            echo '<pre style="padding-bottom: 10px; border-bottom:1px dotted #CC4141; color:#CC4141; line-height:1.15em;"><strong>'. $location . $typeString  . '</strong><br>';
            foreach ($pieces as $key => $value) {
                if (is_array($value)) {
                    getHandleElements::printArray($value, 1);
                } else {
                    echo '<strong style="padding-right:5px; width:140px; text-align:right; display:inline-table; border-right:2px solid #CC4141;">'. $key .'</strong><span style="color:#CC4141;">=></span> '. $value .'<br>';
                }
            }
            echo $_date.'</pre>';
        }
    }

    public static function printArray($values, $slip) {
        foreach ($values as $key => $value) {
            if (is_array($value)) {
                $slipValue = $slip + 2;
                getHandleElements::printArray($value, $slipValue);
            } else {
                $borderSize = 5 + $slip;
                echo '<strong style="padding-right:'. $borderSize .'px; width:140px; text-align:right; display:inline-table; border-right:'. $borderSize .'px solid #CC4141;">'. $key .'</strong><span style="color:#CC4141;">=></span> '. $value .'<br>';
            }
        }
    }
}
