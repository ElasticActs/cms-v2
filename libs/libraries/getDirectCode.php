<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\libraries;

/**
 * Class getDirectCodeContents
 * @package redBerg\App\controllers
 */
class getDirectCode
{
	public static $php_start;
	public static $php_end;
	public static $errMsg;

	public function __construct() {
		self::$php_start = "<?php";
		self::$php_end   = "?>";
	}

	public function compiledCode($text) {
		$module_file = CONFIG_ROOT . "/public/modules/".$text.".inc";
		$contents = file_get_contents($module_file, FILE_USE_INCLUDE_PATH);

		$contents = self::fix_str($contents);
		$output = "";
		$regexp = '/(.*?)'.self::fix_reg(self::$php_start).'\s+(.*?)'.self::fix_reg(self::$php_end).'(.*)/s';

		$found = preg_match($regexp, $contents, $matches);
		while ( $found ){
			$output .= $matches[1];
			$phpCode = $matches[2];

			if ( self::check_php($phpCode) ){
                ob_start();

				eval(self::fix_str2($phpCode));

				$output .= ob_get_contents();
				ob_end_clean();
			} else {
				$output .= "The following command is not allowed: <strong>". self::$errMsg ."</strong>";
			}

			$contents = $matches[3];
			$found = preg_match( $regexp, $contents, $matches );
		}
		$output .= $contents;

		return	$output;
	}

	public static function fix_str($str) {
		$str = str_replace( '{?php', '<?php', $str );
		$str = str_replace( '?}', '?>', $str );
		$str = preg_replace( array( '%&lt;\?php( \s|&nbsp;|<br\s/>|<br>|<p>|</p> )%s', '/\?&gt;/s', '/-&gt;/' ), array( '<?php ', '?>', '->' ), $str );

		return $str;
	}

	public static function fix_str2($str) {
		$str = str_replace( '&#39;', "'", $str );
		$str = str_replace( '&quot;', '"', $str );
		$str = str_replace( '&lt;', '<', $str );
		$str = str_replace( '&gt;', '>', $str );
		$str = str_replace( '&amp;', '&', $str );
		$str = str_replace( '&nbsp;', ' ', $str );
		$str = str_replace( '&#160;', "\t", $str );
		$str = str_replace( chr( hexdec( 'C2' ) ).chr( hexdec( 'A0' ) ), '', $str );
		$str = str_replace( html_entity_decode( "&Acirc;&nbsp;" ), '', $str );

		return $str;
	}

	public static function fix_reg($str) {
		$str = str_replace( '?', '\?', $str );
		$str = str_replace( '{', '\{', $str );
		$str = str_replace( '}', '\}', $str );

		return $str;
	}

	public static function check_php($code) {
		global $Config_enable_command_block, $Config_block_list;

		$status = 1;
		if ( !$Config_enable_command_block ) {
            return $status;
        }

		$function_list = array();

		if ( preg_match_all( '/([a-zA-Z0-9_]+)\s*[(|"|\']/s', $code, $matches ) ) {
			$function_list = $matches[1];
		}

		if ( preg_match( '/`(.*?)`/s', $code ) ) {
			$status	= 0;
			self::$errMsg	= 'backticks ( `` )';

			return $status;
		}

		if ( preg_match( '/\$database\s*->\s*([a-zA-Z0-9_]+)\s*[(|"|\']/s', $code, $matches ) ) {
			$status	= 0;
			self::$errMsg	= 'database->'.$matches[1];

			return $status;
		}

		foreach( $function_list as $command ) {
			if ( in_array( $command, $Config_block_list ) ) {
				$status = 0;
				self::$errMsg	= $command;
				break;
			}
		}

		return $status;
	}
}
