<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\initials;

/**
 * Base Initialization
 */
abstract class getInitialBase
{
    protected $dotenv;
    /**
     * getInitialBase constructor.
     */
    protected function __construct()
    {
        // Load dot env file
        $this->dotenv = \Dotenv\Dotenv::createImmutable(CONFIG_ROOT);
        $this->dotenv->load();

        // Set a liberal script execution time limit
        if (function_exists("set_time_limit") && @ini_get("safe_mode") == 0) {
            @set_time_limit(300);
        }

        ini_set('output_buffering', 'off');

        // Required for IE, otherwise Content-disposition is ignored
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 0);
        }

        // Site With GZIP Compression
        if (substr_count(getenv('HTTP_ACCEPT_ENCODING'), 'gzip')) {
            if (!in_array('ob_gzhandler', ob_list_handlers())) {
                ob_start('ob_gzhandler');
                ob_implicit_flush(0);
            } else {
                ob_start();
            }
        } else {
            ob_start();
        }
    }

    protected function setInitialBase()
    {
        // Prevent Robot access for security
        if ($_SERVER['SERVER_PROTOCOL'] != 'HTTP/1.1' && $_SERVER['SERVER_PROTOCOL'] != 'HTTP/1.0') {
            // Set a 400 (Bad Request) response code and exit.
            header('HTTP/1.1 400 Bad Request');
            die('<h1>No Service available</h1><br>400 Bad Request');
        }
        
        $this->catchErrors();
        $this->setTimeZone(DEFAULT_TIMEZONE);

        // If the short valuable is not supported
        if (isset($HTTP_POST_VARS) && !isset($_POST)) {
            $_POST      =& $HTTP_POST_VARS;
            $_GET       =& $HTTP_GET_VARS;
            $_SERVER    =& $HTTP_SERVER_VARS;
            $_COOKIE    =& $HTTP_COOKIE_VARS;
            $_ENV       =& $HTTP_ENV_VARS;
            $_FILES     =& $HTTP_POST_FILES;

            if (!isset($_SESSION)) {
                $_SESSION =& $HTTP_SESSION_VARS;
            }
        }

        if (!ini_get('register_globals')) {
            @extract($_GET);
            @extract($_POST);
            @extract($_SERVER);
            @extract($_COOKIE);
            @extract($_SESSION);
            @extract($_ENV);
            @extract($_FILES);
        }

        // It will apply addslashes() to protect it from SQL Injection
        if (is_array($_GET)) {
            foreach ($_GET as $key => $value) {
                if (is_array($_GET[$key])) {
                    foreach ($_GET[$key] as $key2 => $value2) {
                        $_GET[$key][$key2] = addslashes($value2);
                    }

                    @reset($_GET[$key]);
                } else {
                    $_GET[$key] = addslashes($value);
                }
            }

            @reset($_GET);
        }

        if (is_array($_POST)) {
            foreach ($_POST as $key => $value) {
                if (is_array($_POST[$key])) {
                    foreach ($_POST[$key] as $key2 => $value2) {
                        $_POST[$key][$key2] = addslashes($value2);
                    }

                    @reset($_POST[$key]);
                } else {
                    $_POST[$key] = addslashes($value);
                }
            }

            @reset($_POST);
        }

        if (is_array($_COOKIE)) {
            foreach ($_COOKIE as $key => $value) {
                if (is_array($_COOKIE[$key])) {
                    foreach ($_COOKIE[$key] as $key2 => $value2) {
                        $_COOKIE[$key][$key2] = addslashes($value2);
                    }

                    @reset($_COOKIE[$key]);
                } else {
                    $_COOKIE[$key] = addslashes($value);
                }
            }

            @reset($_COOKIE);
        }
        
        if (is_array($_SESSION)) {
            foreach ($_SESSION as $key => $value) {
                if (is_array($_SESSION[$key])) {
                    foreach ($_SESSION[$key] as $key2 => $value2) {
                        $_SESSION[$key][$key2] = addslashes($value2);
                    }

                    @reset($_SESSION[$key]);
                } else {
                    $_SESSION[$key] = addslashes($value);
                }
            }

            @reset($_SESSION);
        }

        if (is_array($_SERVER)) {
            foreach ($_SERVER as $key => $value) {
                if (is_array($_SERVER[$key])) {
                    foreach ($_SERVER[$key] as $key2 => $value2) {
                        $_SERVER[$key][$key2] = addslashes($value2);
                    }

                    @reset($_SERVER[$key]);
                } else {
                    $_SERVER[$key] = addslashes($value);
                }
            }

            @reset($_SERVER);
        }

        /**
         * extract( $_GET );
         * Protect from page.php?_POST[var1]=data1&_POST[var2]=data2
         * extract( $_GET ); 명령으로 인해 page.php?_POST[var1]=data1&_POST[var2]=data2
         * 와 같은 코드가 _POST 변수로 사용되는 것을 막음
         */
        $extractArray = array('PHP_SELF', '_ENV', '_GET', '_POST', '_FILES', '_SERVER', '_COOKIE', '_SESSION', '_REQUEST', 'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_SERVER_VARS', 'HTTP_COOKIE_VARS', 'HTTP_SESSION_VARS', 'GLOBALS');
        for ($i = 0; $i < count($extractArray); $i++) {
            if (isset($_GET[$extractArray[$i]])) {
                unset($_GET[$extractArray[$i]]);
            }
        }

        $rg = array_keys($_REQUEST);
        foreach ($rg as $var) {
            if ($_REQUEST[$var] === $$var) {
                unset($$var);
            }
        }
    }
}
