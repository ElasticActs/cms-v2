<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\initials;

/**
 * Initialization of error log and Timezone
 */
class getInitial extends getInitialBase
{
    public function __construct()
    {
        parent::__construct();

        try {
            $path = preg_replace('/\W\w+\s*(\W*)$/', '$1', realpath(dirname(__DIR__)));
            // Require once globals & constants files
            if (file_exists($path . '/libs/configs/config.php')) {
                require_once $path . '/libs/configs/config.php';
            } else {
                throw new \Exception('There is no main file. Please check the config file');
            }
    
            $this->setInitialBase();
    
            // Initial Session Handling
            $this->setHandlingSession(SESSION_SAVE_PATH, CONFIG_COOKIE_DOMAIN);
        } catch (\Exception $exception) {
            echo '<h1>No Service available</h1>';
            echo $exception->getMessage();
            // echo '<br>:: '. dirname(__FILE__) .'/'. basename(__FILE__) .  ' (line: 21)'; 
            header('HT /1.1 503 Service Unavailable - ' . $exception->getMessage());
            die();
        }
    }

    // Handle errors
    public function catchErrors()
    {
        // Setting the level of error reporting
        // Show all types but notices
        ini_set('display_errors', 1);

        error_reporting(E_ALL ^ E_NOTICE);
    }

    // SetUp Timezone with default_timezone
    public function setTimeZone($zone)
    {
        // Setting Default Timezone
        if (date_default_timezone_get() !== $zone) {
            if (function_exists('date_default_timezone_set')) {
                date_default_timezone_set($zone);
            }
        }
    }

    // Handling _SESSION
    public function setHandlingSession($sessionPath, $cookieDomain)
    {
        // Block not to pass PHPSESSID automatically
        @ini_set('session.use_trans_sid', 0);
        // Prohibit to follow URL by PHPSESSID
        @ini_set('url_rewriter.tags', '');
        // Session Cache Expire (Mins)
        @ini_set('session.cache_expire', 180);
        // life time for garbage collection of session data (Secs)
        @ini_set('session.gc_maxlifetime', 10800);
        // session.gc_probability는 session.gc_divisor와 연계하여 gc(쓰레기 수거) 루틴의 시작 확률을 관리합니다.
        // 기본값은 1입니다. 자세한 내용은 session.gc_divisor를 참고
        @ini_set('session.gc_probability', 1);
        // session.gc_divisor는 session.gc_probability와 결합하여
        // 각 세션 초기화 시에 gc(쓰레기 수거) 프로세스를 시작할 확률을 정의합니다.
        // 확률은 gc_probability/gc_divisor를 사용하여 계산합니다.
        // 즉, 1/100은 각 요청시에 GC 프로세스를 시작할 확률이 1%입니다.
        // session.gc_divisor의 기본값은 100입니다.
        @ini_set('session.gc_divisor', 100);
        // To protect session cookie
        @ini_set('session.cookie_httponly', 1);
        @ini_set('session.cookie_secure', 1);

       // session_save_path($sessionPath);
        session_cache_limiter('no-cache, must-revalidate');
        // 세션쿠키가 적용되는 위치 (특별한 경우가 없다면 일반적으로 홈디렉토리 루트경로인 / 를 설정)
        // session_set_cookie_params($expire, $path, $domain, $secure, httponly);
        session_set_cookie_params(0, "/", $cookieDomain, isset($_SERVER["HTTPS"]), true);

        // but not all user allow cookies
        @ini_set('session.use_only_cookies', 'false');
        // delete session/cookies when browser is closed
        @ini_set('session.cookie_lifetime', 0);
        // warn but don't work with bug
        @ini_set('session.bug_compat_42', 'false');
        @ini_set('session.bug_compat_warn', 'true');
        // 세션이활성화될도메인
        @ini_set('session.cookie_domain', $cookieDomain);

        @session_name( md5( CONFIG_SITE_URL ) );
        @session_start();
    }

    // Flush the output buffer and turn off output buffering
    public function flushBuff()
    {
        // This will delete all of files under the /static/filemanager/thumbs/
        // which are temporarily generated from UPLOAD/IMAGE FOLDER
        if (date('d') == 1 || date('d') == 15 || date('d') == 30) {
            $folders = CONFIG_DOCUMENT_ROOT . '/static/filemanager/thumbs/';
        }

        unset($_SESSION['msg']);

        // clearstatcache() to clear the information that PHP caches about a file
        clearstatcache();
        if (ob_get_length()) {
            @ob_end_flush();
            @flush();
        }

        exit();
    }
}
