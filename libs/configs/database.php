<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\configs;

/**
 * Class to return values
 */
trait database
{
    /**
     * Getting allowed file extension
     * @return array
     */
    protected static function dbParams()
    {
        return [
            'DB_SERVER'   => $_ENV['DB_SERVER'],   // This is normally set to localhost
            'DB_USERNAME' => $_ENV['DB_USERNAME'],   // MySQL Username
            'DB_PASSWORD' => $_ENV['DB_PASSWORD'],      // MySQL Password
            'DB_NAME'     => $_ENV['DB_NAME']       // MySQL Database name
        ];
    }
}
