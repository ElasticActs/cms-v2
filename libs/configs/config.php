<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\Libs\configs;

/** ----------------------------------------------------------------------
* Global Configuration
* Setting flag to prevent direct access
* ------------------------------------------------------------------------ */
defined( '_VALID_TAGS' ) or die( 'Your system is not working properly. config' );

/** ----------------------------------------------------------------------
* Define static folder and static domain
*  ----------------------------------------------------------------------- */
define( 'CONFIG_SITE_URL', 'redberg.local' );
define( 'CONFIG_ENABLE_SSL', false);
define( 'ENV_STATUS', 'dev' ); // dev OR production

/** --------------------------------------------------------------------
* Setting for session
* ------------------------------------------------------------------- */
define( 'SESSION_SAVE_PATH', CONFIG_ROOT . '_data/_session' );
define( 'CONFIG_COOKIE_DOMAIN', '.' . CONFIG_SITE_URL );

/** ----------------------------------------------------------------------
* Basic setup for SUB like /sub-foldername
*
* When you have the package under SUB-FOLDER-NAME, need to define.
* define( UNDER_SUBFOLDER, /sub-foldername );
*
* AND need to change TWO files
* IN /static/filemanager/config/config.php
* $upload_dir = '/sub-foldername/upload/images/';
* $current_path = $_SERVER['DOCUMENT_ROOT'].'/sub-foldername/upload/images/';
*
* IN .htaccess
* RewriteRule ^(.*)$ sub-foldername/index.php [E=CI_PATH:/$1,L]
*----------------------------------------------------------------------- */
define( 'UNDER_SUBFOLDER', '' );
define( 'CONFIG_PROTECT_BY_IP', false );

define( 'DEFAULT_TIMEZONE', 'America/Toronto' );

/** ----------------------------------------------------------------------
* Basic Directories
* ------------------------------------------------------------------- */
define( 'CONFIG_DOCUMENT_ROOT', CONFIG_ROOT . 'public/' );
define( 'CONFIG_LOG', CONFIG_ROOT . 'log/' );

/** --------------------------------------------------------------------
* Upload Directories
* ------------------------------------------------------------------- */
define( 'CONFIG_UPLOAD_ROOT', 'upload/' );
define( 'CONFIG_UPLOAD_TEMP', CONFIG_UPLOAD_ROOT . 'temp/' );
define( 'CONFIG_UPLOAD_IMAGES', CONFIG_UPLOAD_ROOT . 'images/' );
define( 'CONFIG_UPLOAD_MUSIC', CONFIG_UPLOAD_ROOT . 'sounds/' );
define( 'CONFIG_UPLOAD_VOD', CONFIG_UPLOAD_ROOT . 'vods/' );
define( 'CONFIG_UPLOAD_DOCS', CONFIG_UPLOAD_ROOT . 'docs/' );

/** ---------------------------------------------------------------------
* File and Directory Modes
*------------------------------------------------------------------------
* These prefs are used when checking and setting modes when working
* with the file system.  The defaults are fine on servers with proper
* security, but you may wish (or even need) to change the values in
* certain environments (Apache running a separate process for each
* user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
* always be used to set the mode correctly.
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/** -----------------------------------------------------------------------
* File Stream Modes
*--------------------------------------------------------------------------
* These modes are used when working with fopen()/popen()
*/
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('CONFIG_NOTICE_EXIST_NO_FILE', 'Specified file does not exist.');

define('CONFIG_NUMBER_OF_ARTICLES_PER_PAGE', 12);
define('CONFIG_NUMBER_OF_PAGES_PER_BLOCK', 12);

$Config_enable_command_block    = 0;

$Config_entities_match			= array( '.','&quot;','!','@','#','$','%','^','&','*','( ',' )','_','+','{','}','|',':','"','<','>','?','[',']','\\',';',"'",',','.','/','*','+','~','`','=','’' );
$Config_entities_replace		= array( '','','','','','','','','','','','','','','','','','','','','','','','','','' );
$Config_vod_icon				= "";