[@header/]
[@topMenu/]
<!-- banner -->
[@topBanner/]
<main class="main-container container">
    <!-- Heading Row -->
    <div class="row no-gutters">
        <div class="col-12 mx-auto">
        [@breadcrumb/]
        </div>
    </div>

    <div class="row no-gutters">
        [@leftSide/]
        <div class="[@mainCss/]">
            <h1 class="borderTitle pageTitle"><a>[@pageTitle/]</a></h1>
            <h2 class="articleTitle">
                <a href="" target="_self" title="[@articleTitle/]" onfocus="this.blur()">[@articleTitle/]</a>
            </h2>
            <div class="authorArea">
                <span class="author">[@author/]</span>
                <span class="wdate">[@displayDate/]</span>
                <span class="counts"> <span class="read">VIEW <span class="num">[@views/]</span></span>
            </div>
            [@filenames/]
            [@outboundLinks/]
            [@displayableFile/]
            [@embedCode/]
            <div class="fullTxt">[@fulltxt/]</div>
            <div class="audioFile">[@audioFile/]</div>
            [@boardList/]
        </div>
        [@rightSide/]
    </div>
</main>
[@bottomBanner/]
[@bottom/]
[@footer/]
