[@header/]
[@topMenu/]
<!-- banner -->
[@topBanner/]
<main class="main-container container">
    <!-- Heading Row -->
    <div class="row no-gutters">
        <div class="col-12 mx-auto">
            <!-- breadcrumb -->
            [@breadcrumb/]
        </div>
    </div>

    <!-- Main Row -->
    <div class="row no-gutters">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">

            <div class="card card-signin my-5">
                <div class="card-body">
                    <form id="form-signin">
                        <h1 class="h3 mb-3 font-weight-bold">Log in</h1>
                        
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input autocomplete="off" type="email" id="loginEmail" class="form-control signinEmail" placeholder="Email address" required>

                        <label for="inputPassword" class="sr-only">Password</label>
                        <input autocomplete="off" type="password" id="loginPassword" class="form-control signinPassword" placeholder="Password" required>

                        <br/>
                        <button onfocus='this.blur()' class="btn btn-sm btn-primary btn-block" type="submit" id="loginBtn">Log in</button>
                    </form>

<a type="button" data-toggle="modal" data-target="#logOutModal">
  Log out
</a>
                    <div id="loginMsg"></div>
                </div>
            </div>
            
        </div>
    </div>
    [@user3/]
</main>
[@bottomBanner/]

[@bottom/]
[@footer/]
