    <a href="#" class="go-to-top" style="display: none;">TOP</a>

    <!-- logout Modal -->
    <div id="logOutModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirmToLogOut" aria-hidden="true">
        <div class="modal-dialog modal-confirm modal-dialog-centered" role="document"">
            <div class="modal-content">
                <div class="modal-header flex-column">					
                    <h4 class="modal-title w-100 font-weight-bold text-center" id="confirmToLogOut"><span id="modalName"></span>Are you sure?</h4>                   
                </div>
                <div class="modal-body">
                    <p>Do you really want to log out?</p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal" onfocus='this.blur()'>Cancel</button>
                    <button type="button" class="btn btn-sm btn-info" id="confirmedLogOut" onfocus='this.blur()'>Log Out</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <script type="text/javascript" src="/assets/js/main.js"></script>
    <script type="text/javascript" src="/assets/js/lean_slider.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
                var slider = $('#slider-area').leanSlider({
                controlNav: '#slider-control-nav'
            });
        });
    </script>

    [@footJs/]
    [@footTracking/]    
  </body>
</html>
