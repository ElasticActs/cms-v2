[@header/]
[@topMenu/]
<main class="container h-100 notFound-container">
    <!-- Heading Row -->
    <div class="row align-items-center">
        <div class="col-12 text-center ">
            <h1 class="text-muted">404 Error</h1>
            <h3 class="text-muted">We're sorry the page you requested could not be found.</h3>
            <h4 class="text-danger">redBergs!</h4>
        </div>
    </div>
</main>

[@footerMenu/]
[@footer/]
