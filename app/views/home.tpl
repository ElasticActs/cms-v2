[@header/]
[@topMenu/]
<!-- banner -->
[@topBanner/]
<main class="main-container container">
    <!-- Heading Row -->
    <div class="row no-gutters">
        <div class="col-12 mx-auto">
            <!-- breadcrumb -->
            [@breadcrumb/]
        </div>
    </div>

    <!-- Main Row -->
    <div class="row no-gutters">
        <!-- Left -->
        [@leftSide/]

        <!-- Center -->
        <div class="[@mainCss/]">
            <div class="fullTxt">[@fulltxt/]</div>
        </div>

        <!-- right -->
        [@rightSide/]
    </div>
    [@user3/]
</main>
[@bottomBanner/]

[@bottom/]
[@footer/]
