<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="origin" name="Referrer">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta id="extViewportMeta" name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="google-site-verification" content=""/>
    <meta name="robots" content="index,follow"/>

    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://stackpath.bootstrapcdn.com/">
    <link rel="dns-prefetch" href="https://use.fontawesome.com/">

    <!-- Bootstrap CSS fonts.gstatic-->
    <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin />
    <link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&subset=korean&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800&subset=korean&display=swap" media="print" onload="this.media='all'" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/styles/adminStyle.css">

    [@page_css/]

    <title>redBergs! administration</title>
    <meta name="description" content="[@metaDescription/]">
    <meta name="author" content="elasticActs">
    <meta name="generator" content="redBergs">
    <link rel="canonical" href="[@canonicalLink/]">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/assets/images/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/assets/images/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/assets/images/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/assets/images/favicons/manifest.json">
    <link rel="mask-icon" href="/assets/images/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/favicon.ico">

    <meta name="msapplication-config" content="/assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">


    [@topJs/]
    [@headTracking/]
</head>
<body class="d-flex flex-column h-100" id="adminBody">
