[@adminHeader/]
[@adminMenu/]

<main class="main-container container">
    <!-- Main Row -->
    <div class="row no-gutters">
        <!-- Left -->
        [@adminLeft/]

        <!-- Center -->
        <div class="[@mainCss/]">
            [@list/]
        </div>

    </div>
    [@user3/]
</main>

[@adminBottom/]
[@adminFooter/]
