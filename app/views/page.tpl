[@header/]
[@topMenu/]
<!-- banner -->
[@topBanner/]
<main class="main-container container">
    <!-- Heading Row -->
    <div class="row no-gutters">
        <div class="col-12 mx-auto">
            [@breadcrumb/]
        </div>
    </div>

    <div class="row no-gutters">
        [@leftSide/]
        <div class="[@mainCss/]">
            <h1 class="borderTitle pageTitle"><a>[@title/]</a></h1>
            [@filenames/]
            [@displayableFile/]
            <div class="fullTxt">[@fulltxt/]</div>
            <div class="audioFile">[@audioFile/]</div>
            [@boardList/]
        </div>
        [@rightSide/]
    </div>

    [@user3/]
</main>
[@bottomBanner/]

[@bottom/]
[@footer/]
