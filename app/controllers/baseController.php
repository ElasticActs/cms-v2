<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class baseContents
 * @package redBerg\App\controllers
 */
class baseController
{
    use \redBerg\Libs\libraries\getHandleElements;

    protected $log;
    protected $routeURI = array();
    protected $user;
    protected $allModules;
    protected $allBanners;
    protected $view;
    protected $render;
    protected $topPortion;
    protected $pageInfo;

    /**
     * baseController constructor.
     * @param $uri
     */
    protected function __construct(array $uri)
    {
        $this->routeURI   = $uri;
        $this->allBanners = new \redBerg\App\controllers\bannerController($this->routeURI);
        $this->allModules = new \redBerg\App\controllers\moduleController($this->routeURI);
        $this->log        = new \redBerg\Libs\libraries\getWriteLog();
        $this->view       = new \redBerg\Libs\libraries\getViews();
        $this->render     = new \redBerg\Libs\libraries\getRenders();
        $this->user       = new \redBerg\App\models\memberModel();
        $this->pageInfo   = array();
    }

    /**
     * 
     */
    protected function getTopPortion(array $moduleContents) 
    {
        foreach ($moduleContents as $key => $value) {
            foreach ($value as $secondKey => $secondValue) {
                if ($secondKey == 'top_menu') {
                    $this->topPortion['topMenu'] = $secondValue;
                    unset($moduleContents[$key]['top_menu']);
                }

                if ($secondKey == 'breadcrumb') {
                    $this->topPortion['breadcrumb'] = $secondValue;
                    unset($moduleContents[$key]['breadcrumb']);
                }

                if ($secondKey == 'footer') {
                    $this->topPortion['footerMenu'] = $secondValue;
                    unset($moduleContents[$key]['footer']);
                }                
            }
        }
    }
}
