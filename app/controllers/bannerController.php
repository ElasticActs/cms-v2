<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class home
 * @package ElasticActs\App\controllers
 */
class bannerController
{
    private $log;
    private $banner;
    private $render;
    private $allBanner;
    private $fullBanner;

    /**
     * homeController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        $this->routeURI   = $uri;        
        $this->banner     = new \redBerg\App\models\bannerModel();
        $this->log        = new \redBerg\Libs\libraries\getWriteLog();
        $this->render     = new \redBerg\Libs\libraries\getRenders();
        $this->allBanner  = null;
        $this->fullBanner = array();
    }

    /**
     * @param null
     */
    public function getBanners($pageId, $indicator)
    {
        if ($this->banner->getCountAllBannerPerPage($pageId) > 0) {

            $this->allBanner = $this->banner->getAllBannerPerPage($pageId);
            foreach ($this->allBanner as $value) {
                $this->fullBanner[$value['position']] = $this->render->renderBanner(
                    $value['filename'], 
                    $value['fulltxt'], 
                    $value['title'], 
                    $value['urls'], 
                    $value['title'], 
                    $value['target_window'], 
                    $value['extra_class'], 
                    $indicator, 
                    false);
            }

            return $this->fullBanner;
        }
    }
}
