<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class moduleBaseContents
 * @package redBerg\App\controllers
 */
class moduleBaseController
{
    use \redBerg\Libs\libraries\getHandleElements;

    /**
     * @var getWriteLog
     */
    private $log;

    /**
     * @var array
     */
    public $routeURI = array();

    /**
     * @var baseModel
     */
    private $base;

    private $menu;

    private $newLink;

    private $breadcrumbURI;

    private $breadcrumb;

    private $breadcrumbInfo;

    private $menuArray;

    /**
     * baseController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        $this->routeURI = $uri;
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
        $this->base = new \redBerg\App\models\baseModel();
        $this->newLink = '';
        $this->breadcrumbURI = '';
        $this->breadcrumbInfo = array();
        $this->menuArray = array();
    }

    /**
     * @param null $type
     * @return array
     */
    public function getMenu($type = null)
    {
        $this->menu = $this->base->getMenu($type);

        return $this->menu;
    }

    /**
     * @param array $uri
     * @return array
     */
    public function getBreadcrumb($uri)
    {
        $this->breadcrumbInfo[] = array(
            'link' => '/',
            'menuName' => 'Home'
        );

        if (isset($uri['uri'])) {
            $this->menuArray	= explode("/", $uri['uri']);
            foreach ($this->menuArray as $key => $value) {
                $this->breadcrumbURI .= "/".$value;
                $this->breadcrumb = $this->base->getMenuInfo($this->breadcrumbURI);

                if (count($this->breadcrumb) == 0) {
                    break;
                }

                if (count($this->breadcrumb) > 0) {
                    foreach ($this->breadcrumb as $value) {
                        $this->newLink = self::isEmpty($value['link']) ? $value['alias'] : $value['link'];
                    }
                }

                $this->breadcrumbInfo[] = array(
                    'link' => $this->newLink,
                    'menuName' => $value['mtitle']
                );
            }

            if (isset($uri['ino']) && !self::isEmpty($uri['ino'])) {
                $this->breadcrumbInfo[] = array(
                    'link' => $uri['uri'] . '/' . $uri['ino'],
                    'menuName' => 'Current'
                );
            }
        }

        return $this->breadcrumbInfo;
    }
}
