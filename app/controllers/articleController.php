<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class articleContents
 * @package redBerg\App\controllers
 */
class articleController extends baseController
{
    private $page;
    private $article;
    private $contents;
    private $bannerContents;
    private $moduleContents;
    private $afterRenderContents;
    private $articleInfo;
    private $home;
    private $homeInfo;

    /**
     * pageController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        $this->home = new \redBerg\App\models\homeModel();
        $this->page    = new \redBerg\App\models\pageModel();
        $this->article = new \redBerg\App\models\articleModel();
    }

    /**
     * @param null
     */
    public function getArticle()
    {
        $this->contents = $this->mainContent($this->routeURI);

        $this->bannerContents = $this->allBanners->getBanners($this->contents['pageId'], 0);

        $this->moduleContents = $this->allModules->getModules($this->contents['pageId']);

        $this->getTopPortion($this->moduleContents);

        $this->afterRenderContents = $this->render->renderFullContents(
            array(
                'allBanners' => $this->bannerContents, 
                'allModules' => $this->moduleContents, 
                'contents' => $this->contents)
        );

        $this->view->view('article', array_merge($this->topPortion, $this->afterRenderContents));
    }

    /**
     * 
     */
    private function setArticleNotFound(int $code)
    {
        $this->homeInfo = $this->home->getHomePage();
        $this->getTopPortion($this->allModules->getModules($this->homeInfo['id']));

        http_response_code($code);
        $this->view->view('404', array(
            'topMenu' => $this->topPortion['topMenu'], 
            'footerMenu' => $this->topPortion['footerMenu'])
        );

        exit();
    }

    /**
     * @param array $uri
     * @return array
     */
    private function mainContent(array $uri) : array
    {
        $this->pageInfo = $this->page->getPage($uri);

        //[ino] => 04149601
        $this->articleInfo = $this->article->getArticle($uri['ino']);

        if (self::checkKeyExist('error', $this->articleInfo)) {
            $this->setArticleNotFound($this->articleInfo['code']);
        }

        $this->articleInfo['pageId'] = $this->pageInfo['id'];
        unset($this->pageInfo['id']);

        $this->articleInfo['metaTitle'] = $this->pageInfo['metatitle'];
        unset($this->pageInfo['metatitle']);

        $this->articleInfo['metaKey'] = $this->pageInfo['metakey'];
        unset($this->pageInfo['metakey']);

        $this->articleInfo['metaDescription'] = $this->pageInfo['metadesc'];
        unset($this->pageInfo['metadesc']);

        $this->articleInfo['canonicalLink'] = '/' . $uri['uri'] .'/'. $uri['ino'];

        $this->articleInfo['pageTitle'] =  $this->pageInfo['title'];

        if (!$this->pageInfo['showtitle'] && !self::isEmpty($this->pageInfo['title_images'])) {
            $this->articleInfo['title'] = $this->render->renderImg($this->pageInfo['title_images'], $this->pageInfo['title']);
        }

        // if (!self::isEmpty($this->pageInfo['banner_images'])) {
        //     $this->articleInfo['bannerImages'] = $this->render->renderImg($this->pageInfo['banner_images'], $this->pageInfo['title']);
        // }

        $this->articleInfo['metaTitle'] = !self::isEmpty($this->articleInfo['metatitle']) ? $this->articleInfo['metatitle'] : $this->articleInfo['title'];

        $this->articleInfo['metaKey'] = $this->articleInfo['metakey'];

        $this->articleInfo['metaDescription'] = $this->articleInfo['metadesc'];
        if (self::isEmpty($this->articleInfo['metadesc'])) {
            $this->articleInfo['metaDescription'] = self::isEmpty($this->articleInfo['summarytxt']) ? $this->articleInfo['title'] : $this->articleInfo['summarytxt'];
        }

        $this->articleInfo['articleTitle'] = $this->articleInfo['title'];

        $userInfo = $this->user->getUser($this->articleInfo['created_by']);

        $this->articleInfo['author'] = $userInfo['members_alias'];
        if (!self::isEmpty($this->articleInfo['host'])) {
            $this->articleInfo['author'] = $this->articleInfo['host'];
            if (!self::isEmpty($this->articleInfo['host'])) {
                $this->articleInfo['author'] .= '&nbsp;&nbsp;['. $this->articleInfo['casting_description'] .']';
            }
        }

        $this->articleInfo['views'] = $this->articleInfo['views'];

        $this->articleInfo['displayDate'] = self::getShortFormatDate($this->articleInfo['casting_date']);
        if ($this->articleInfo['casting_date'] == '0000-00-00') {
            $this->articleInfo['displayDate'] = self::getShortFormatDate($this->articleInfo['publish_date']);
        }

        if (!self::isEmpty($this->articleInfo['urls'])) {
            $this->articleInfo['outboundLinks'] = $this->render->renderOutboundLink($this->articleInfo['urls']);
        }

        if (!self::isEmpty($this->articleInfo['filename'])) {
            $renderedFile = $this->render->renderFiles($this->articleInfo['filename']);

            $this->articleInfo['filenames']       = $renderedFile['download'];
            $this->articleInfo['displayableFile'] = $renderedFile['display'];
            $this->articleInfo['audioFile']       = $renderedFile['audio'];
        }

        if (!self::isEmpty($this->articleInfo['embed_code'])) {
            $this->articleInfo['embedCode'] = $this->render->renderEmbed($this->articleInfo['embed_code']);
        }

        if (!self::isEmpty($this->articleInfo['embed_bigcode'])) {
            $this->articleInfo['embedCode'] = $this->render->renderEmbed($this->articleInfo['embed_bigcode']);
        }

        $this->articleInfo['fulltxt'] = $this->articleInfo['fulltxt'];

        return $this->articleInfo;
    }
}
