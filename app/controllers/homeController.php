<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class homeContents
 * @package redBerg\App\controllers
 */
class homeController extends baseController
{
    private $home;
    private $contents;
    private $bannerContents;
    private $moduleContents;
    private $afterRenderContents;

    /**
     * homeController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        $this->home = new \redBerg\App\models\homeModel();
    }

    /**
     * @param null
     */
    public function getContent()
    {
        $this->contents = $this->homeContent();
        $this->bannerContents = $this->allBanners->getBanners($this->contents['pageId'], 1);
        $this->moduleContents = $this->allModules->getModules($this->contents['pageId']);

        $this->getTopPortion($this->moduleContents);

        $this->afterRenderContents = $this->render->renderFullContents(
            array(
                'allBanners' => $this->bannerContents, 
                'allModules' => $this->moduleContents, 
                'contents' => $this->contents)
        );

        $this->view->view('home', array_merge($this->topPortion, $this->afterRenderContents));
    }

    /**
     * @return array
     */
    private function homeContent() : array
    {
        $this->pageInfo = $this->home->getHomePage();

        $this->pageInfo['pageId'] = $this->pageInfo['id'];
        unset($this->pageInfo['id']);

        $this->pageInfo['metaTitle'] = $this->pageInfo['metatitle'];
        unset($this->pageInfo['metatitle']);

        $this->pageInfo['metaKey'] = $this->pageInfo['metakey'];
        unset($this->pageInfo['metakey']);

        $this->pageInfo['metaDescription'] = $this->pageInfo['metadesc'];
        unset($this->pageInfo['metadesc']);

        $this->pageInfo['canonicalLink'] = '/';

        // Inserts HTML line breaks before all newlines in a string
        $this->pageInfo['fulltxt'] = ($this->pageInfo['fulltxt']);

        return $this->pageInfo;
    }
}
