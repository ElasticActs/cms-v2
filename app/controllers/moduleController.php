<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class moduleContents
 * @package redBerg\App\controllers
 */
class moduleController extends moduleBaseController
{
    private $app;
    private $render;
    private $directCode;
    private $modules;
    private $title;
    private $moduleContents;
    private $fullContents;
    private $doneFromController;
    private $breadcrumb;
    private $menuPosition;
    private $menu;
    private $callFromDb;

    /**
     * homeController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        $this->app        = new \redBerg\App\models\moduleModel();
        $this->render     = new \redBerg\Libs\libraries\getRenders();
        $this->directCode = new \redBerg\Libs\libraries\getDirectCode();
        $this->callFromDb = array('top_menu', 'breadcrumb', 'left_menu', 'right_menu', 'footer_menu');
        $this->modules = null;
        $this->title = null;
        $this->moduleContents = array();
        $this->fullContents = null;
        $this->doneFromController = null;
    }

    /**
     * @param null
     */
    public function getModules(int $pageId) : array
    {
        if ($this->app->getCountAllModulesPerPage($pageId) > 0) {
            $this->modules = $this->app->getAllModulesPerPage($pageId);
            foreach ($this->modules as $value) {
                if ($value['showtitle']) {
                    $this->title = $value['title'];
                }

                if (in_array($value['app_position'], $this->callFromDb)) {
                    if ($value['app_position'] == 'breadcrumb') {
                        $this->doneFromController = $this->pageBreadcrumb($this->routeURI);
                    } else {
                        $this->doneFromController = $this->allMenu($value['app_position']);
                    }

                    array_push($this->moduleContents, array($value['app_position'] => $this->doneFromController));

                    continue;
                }

                if (self::isEmpty($value['filename'])) {
                    $this->fullContents = $value['fulltxt'];
                } else {
                    $this->fullContents = $this->directCode->compiledCode($value['filename']);
                }

                array_push($this->moduleContents, array($value['app_position'] => $this->fullContents));
            }

            return $this->moduleContents;
        }
    }

    /**
     * @return string
     */
    public function allMenu(string $menuPosition) : string
    {
        $this->menuPosition = $menuPosition == 'top_menu' ? 'topmenu' :  $menuPosition ;
        $this->menu = $this->getMenu($this->menuPosition);

        if (!self::isEmpty($this->menu)) {
            return $this->render->renderMenu($this->menuPosition, $this->menu);
        }
    }

    /**
     * @param array $uri
     * @return string
     */
    public function pageBreadcrumb(array $uri) : string
    {
        $this->breadcrumb = $this->getBreadcrumb($uri);

        return $this->render->renderBreadcrumb($this->breadcrumb);
    }
}
