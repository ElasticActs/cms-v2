<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

class boardController extends baseController
{
    private $board;
    private $sectionInfo;
    private $categoryInfo;
    private $categoryType;
    private $boardList;
    private $boardInfo;
    /**
     * boardController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        $this->board = new \redBerg\App\models\boardModel();
        $this->sectionInfo = array();
        $this->categoryInfo = array();
        $this->categoryType = ['casting', 'castingthumblist'];
        $this->boardInfo = array();
    }

    public function checkSectionCategory(int $sid, int $cid)
    {
        return $this->board->getCheckSectionCategory($sid, $cid);
    }

    /**
     * @param int $id
     * @return array|mixed
     */
    public function sectionInfo(int $id) : array
    {
        if ($this->board->getSectionInfo($id)) {
            $this->sectionInfo = $this->board->getSectionInfo($id);
        }

        return $this->sectionInfo;
    }

    /**
     * @param int $id
     * @return array|mixed
     */
    public function categoryInfo(int $id) : array
    {
        if ($this->board->getCategoryInfo($id)) {
            $this->categoryInfo = $this->board->getCategoryInfo($id);
        }

        return $this->categoryInfo;
    }

    public function getBoardList(string $type, int $sid, int $cid) : array
    {
        $this->boardList = $this->board->getBoardList($sid, $cid);

        if (!in_array($type, $this->categoryType)) {
            for ($i=0; $i <= count($this->boardList); $i++) {
                unset($this->boardList[$i]['host']);
                unset($this->boardList[$i]['casting_date']);
                unset($this->boardList[$i]['casting_description']);
                unset($this->boardList[$i]['embed_bigcode']);
            }
        }

        return $this->setPagination($this->boardList, CONFIG_NUMBER_OF_ARTICLES_PER_PAGE);
    }

    private function setPagination(array $list, int $unit) : array
    {
        $this->boardInfo['total_item'] = count($list);
        $this->boardInfo['total_page'] = ceil( count($list)/$unit );
        $this->boardInfo['data'] = array_chunk($list, $unit);

        return $this->boardInfo;
    }

}
