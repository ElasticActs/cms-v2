<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class xmlContents
 * @package redBerg\App\controllers
 */
class xmlController extends baseController
{
    /**
     * xmlController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);
    }

    /**
     * @param null
     */
    public function getContent()
    {

    }

}
