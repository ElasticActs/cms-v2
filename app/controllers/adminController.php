<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class adminContents
 * @package redBerg\App\controllers
 */
class adminController extends adminBaseController
{
    private $category;
    private $categoryInfo;
    private $result;
    public $uriElements = array();

    /**
     * backController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        $this->category = new \redBerg\App\models\categoryModel();
        $this->categoryInfo = new \redBerg\App\controllers\categoryController();
    }

    /**
     * @param null
     */
    public function getContent(int $id = null)
    {

        // print_r(count($this->categoryInfo->getList()));
        // echo "<pre>"; print_r($this->categoryInfo->getList()); echo "</pre>";

        $pageNumber = $this->routeURI['pno'] ? $this->routeURI['pno'] - 1 : 0;
        $this->result = $this->render->renderList('category', $this->categoryInfo->getList(), $pageNumber, $this->routeURI['uri'] );
        
        // print_r($this->routeURI);

        $this->scriptInfo = array(
            'page_css' => '',
            'footJs' => ''
        );

        $this->view->view('adminHome', array_merge($this->scriptInfo, array('list' => $this->result)));
    }

    public function getSectionInfo()
    {

    }

    public function getMenuInfo()
    {

    }

    public function getPageInfo()
    {

    }

    public function getArticleInfo()
    {

    }
}
