<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class pageContents
 * @package redBerg\App\controllers
 */
class pageController extends baseController
{
    private $page;
    private $board;
    private $contents;
    private $bannerContents;
    private $moduleContents;
    private $afterRenderContents;
    private $home;
    private $homeInfo;

    /**
     * pageController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        $this->home = new \redBerg\App\models\homeModel();
        $this->page = new \redBerg\App\models\pageModel();
    }

    /**
     * @param null
     */
    public function getContent()
    {
        $this->contents = $this->mainContent($this->routeURI);

        $this->bannerContents = $this->allBanners->getBanners($this->contents['pageId'], 0);

        $this->moduleContents = $this->allModules->getModules($this->contents['pageId']);
        
        $this->getTopPortion($this->moduleContents);

        $this->afterRenderContents = $this->render->renderFullContents(
            array(
                'allBanners' => $this->bannerContents, 
                'allModules' => $this->moduleContents, 
                'contents' => $this->contents)
        );

        $this->view->view('page', array_merge($this->topPortion, $this->afterRenderContents));
    }

    /**
     * 
     */
    private function setPageNotFound(int $code)
    {
        $this->homeInfo = $this->home->getHomePage();
        $this->getTopPortion($this->allModules->getModules($this->homeInfo['id']));

        http_response_code($code);
        $this->view->view('404', array(
            'topMenu' => $this->topPortion['topMenu'], 
            'footerMenu' => $this->topPortion['footerMenu'])
        );

        exit();
    }

    /**
     * @param array $uri
     * @return array
     */
    private function mainContent(array $uri) : array
    {
        //[ino] => 04149601
        //[pno] => 3
        $this->pageInfo = $this->page->getPage($uri);

        if (self::checkKeyExist('error', $this->pageInfo)) {
            $this->setPageNotFound($this->pageInfo['code']);
        }

        $this->pageInfo['pageId'] = $this->pageInfo['id'];
        unset($this->pageInfo['id']);

        $this->pageInfo['metaTitle'] = $this->pageInfo['metatitle'];
        unset($this->pageInfo['metatitle']);

        $this->pageInfo['metaKey'] = $this->pageInfo['metakey'];
        unset($this->pageInfo['metakey']);

        $this->pageInfo['metaDescription'] = $this->pageInfo['metadesc'];
        unset($this->pageInfo['metadesc']);

        $this->pageInfo['canonicalLink'] = '/' . $uri['uri'];

        if (!$this->pageInfo['showtitle'] && !self::isEmpty($this->pageInfo['title_images'])) {
            $this->pageInfo['title'] = $this->render->renderImg($this->pageInfo['title_images'], $this->pageInfo['title']);
        }

        if (!self::isEmpty($this->pageInfo['banner_images'])) {
            $this->pageInfo['bannerImages'] = $this->render->renderImg($this->pageInfo['banner_images'], $this->pageInfo['title']);
        }

        if (!self::isEmpty($this->pageInfo['embed_code'])) {
            $this->pageInfo['embedCode'] = $this->render->renderEmbed($this->pageInfo['embed_code']);
        }

        if (!self::isEmpty($this->pageInfo['filenames'])) {
            $renderedFile = $this->render->renderFiles($this->pageInfo['filenames']);

            $this->pageInfo['filenames']       = $renderedFile['download'];
            $this->pageInfo['displayableFile'] = $renderedFile['display'];
            $this->pageInfo['audioFile']       = $renderedFile['audio'];
        }

        // Inserts HTML line breaks before all newlines in a string
        $this->pageInfo['fulltxt'] = ($this->pageInfo['fulltxt']);

        if (!self::isEmpty($this->pageInfo['sectionid']) && !self::isEmpty($this->pageInfo['categoriesid'])) {
            
            $this->board = new \redBerg\App\controllers\boardController($uri);
            if ($this->board->checkSectionCategory($this->pageInfo['sectionid'], $this->pageInfo['categoriesid'])) {
                $pageNumber   = $uri['pno'] ? $uri['pno'] - 1 : 0;
                $categoryInfo = $this->board->categoryInfo($this->pageInfo['categoriesid']);
                $boardList    = $this->board->getBoardList($categoryInfo['ctype'], $this->pageInfo['sectionid'], $this->pageInfo['categoriesid']);

                $this->pageInfo['boardList'] = $this->render->renderBoardList($categoryInfo['ctype'], $boardList, $pageNumber, $uri['uri']);
            }
        }

        return $this->pageInfo;
    }  
}
