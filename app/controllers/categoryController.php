<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class categoryContents
 * @package redBerg\App\controllers
 */
class categoryController
{
    private $log;
    private $category;
    private $list;
    private $listInfo;

    public $uriElements = array();

    /**
     * backController constructor.
     * @param $uri
     */
    public function __construct()
    {
        $this->log      = new \redBerg\Libs\libraries\getWriteLog();
        $this->category = new \redBerg\App\models\categoryModel();
    }

    public function getList() : array
    {
        $this->list = $this->category->getCategories();

        return $this->setPagination($this->list, CONFIG_NUMBER_OF_ARTICLES_PER_PAGE);
    }

    private function setPagination(array $list, int $unit) : array
    {
        $this->listInfo['total_item'] = count($list);
        $this->listInfo['total_page'] = ceil( count($list)/$unit );
        $this->listInfo['data'] = array_chunk($list, $unit);

        return $this->listInfo;
    }    
}
