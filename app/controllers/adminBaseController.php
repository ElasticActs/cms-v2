<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class adminBaseContents
 * @package redBerg\App\controllers
 */
class adminBaseController
{
    use \redBerg\Libs\libraries\getHandleElements;
    use \redBerg\Libs\libraries\getHandleSession;

    protected $log;
    protected $routeURI = array();
    protected $user;
    protected $view;
    protected $render;
    protected $topPortion;
    protected $pageInfo;
    protected $allModules;
    protected $allBanners;

    /**
     * baseController constructor.
     * @param $uri
     */
    protected function __construct(array $uri)
    {
        $this->routeURI   = $uri;
        $this->log        = new \redBerg\Libs\libraries\getWriteLog();
        $this->view       = new \redBerg\Libs\libraries\getViews();
        $this->render     = new \redBerg\Libs\libraries\getRenders();
        $this->user       = new \redBerg\App\models\memberModel();
    }

    /**
     * 
     */
    protected function getTopPortion(array $moduleContents) 
    {
        foreach ($moduleContents as $key => $value) {
            foreach ($value as $secondKey => $secondValue) {
                if ($secondKey == 'top_menu') {
                    $this->topPortion['topMenu'] = $secondValue;
                    unset($moduleContents[$key]['top_menu']);
                }

                if ($secondKey == 'breadcrumb') {
                    $this->topPortion['breadcrumb'] = $secondValue;
                    unset($moduleContents[$key]['breadcrumb']);
                }

                if ($secondKey == 'footer') {
                    $this->topPortion['footerMenu'] = $secondValue;
                    unset($moduleContents[$key]['footer']);
                }                
            }
        }
    }
}
