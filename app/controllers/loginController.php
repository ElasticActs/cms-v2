<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class loginContents
 * @package redBerg\App\controllers
 */
class loginController extends baseController
{
    use \redBerg\Libs\libraries\getHandleEmail;

    private $home;
    private $contents;
    private $bannerContents;
    private $moduleContents;
    private $afterRenderContents;
    private $member;
    private $memberInfo;
    private $returnValue;
    private $pw;

    /**
     * backController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);

        // $this->uriElements = array();   
        $this->home = new \redBerg\App\models\homeModel();   
        $this->member = new \redBerg\App\models\memberModel();
        $this->pw = new \redBerg\Libs\libraries\getHandlePasswords();
        $this->memberInfo = array();
    }

    /**
     * @param null
     */
    public function getLogin()
    {
        $this->contents = $this->home->getHomePage();
        $this->bannerContents = $this->allBanners->getBanners($this->contents['id'], 1);
        $this->moduleContents = $this->allModules->getModules($this->contents['id']);

        $this->getTopPortion($this->moduleContents);

        $this->afterRenderContents = $this->render->renderFullContents(
            array(
                'allBanners' => $this->bannerContents, 
                'allModules' => $this->moduleContents, 
                'contents' => $this->contents)
        );

        $this->scriptInfo = array(
            'page_css' => '<link rel="stylesheet" href="/assets/styles/login.css">',
            'footJs' => '<script type="text/javascript" src="/assets/js/login.js"></script>'
        );

        $this->view->view('login', array_merge($this->topPortion, $this->afterRenderContents, $this->scriptInfo));
        print_r($_SERVER['HTTP_REFERER']);
    }

    public function processLogin()
    { // tconnectchurch@gmail.com
        try {
            if ( self::getCheckEmail( $this->routeURI['globalValue']['email'] )) {
                $userEmail = stripslashes( strtolower( $this->routeURI['globalValue']['email'] ) );
                $userEmail = str_replace( "@", "&#64;", trim( $userEmail ) );

                $password = stripslashes( $this->routeURI['globalValue']['password'] );

                if (self::isEmpty($password)) {
                    throw new \Exception('Password cannot be empty');
                }

                $this->memberInfo = $this->member->getMemberByEmail($userEmail);   
                
                if (count($this->memberInfo) > 1 || count($this->memberInfo) == 0) {
                   echo json_encode(array('code' => 300, 'message' => 'Sorry, we couldn\'t find an account with that email.'));

                   return;
                }

                if ($this->pw->validatePassword($password, $this->memberInfo[0]['members_password'])) {
                    $loginTime = time();

                    $session_id	= md5( $this->memberInfo[0]['id'] . $this->memberInfo[0]['members_email'] . $loginTime );
    
                    $_SESSION['session_id'] 	    = $session_id;
                    $_SESSION['session_userEmail']  = $this->memberInfo[0]['members_email'];
                    $_SESSION['session_userName']   = $this->memberInfo[0]['members_firstname'];
                    $_SESSION['session_userType']   = $this->memberInfo[0]['members_type'];
                    $_SESSION['session_userLevel']  = $this->memberInfo[0]['members_level'];
                    $_SESSION['session_gid'] 		= $this->memberInfo[0]['members_group_id'];
                    $_SESSION['session_time'] 		= $loginTime;
    
                    session_write_close();
    
                    echo json_encode(array_merge(array('code' => 200, 'message' => 'OK', 'email' => $userEmail), $this->routeURI['globalValue']));

                    return;
                }

                echo json_encode(array_merge(array('code' => 300, 'message' => 'Sorry, that password isn\'t right.', 'email' => $userEmail), $this->routeURI['globalValue']));
            }

            throw new \Exception('There is no controller');

        } catch (\Exception $exception) {
            header('HT /1.1 400 Bad Request - ' . $exception->getMessage());
        }
    }

    public function getLogout()
    {
        if (isset($_SESSION['session_id']) && !self::isEmpty($_SESSION['session_id'])) {
			if( isset( $_COOKIE[session_name()] ) ){
                setcookie( session_name(), "", time()-42000, '/' );
             }

            unset($_SESSION);
            session_destroy();
            
            self::redirectURL('/');
        }
    }

    public function getResetPassword()
    {

    }    
}
