<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\controllers;

/**
 * Class jsonContents
 * @package redBerg\App\controllers
 */
class jsonController extends baseController
{
    /**
     * jsonController constructor.
     * @param $uri
     */
    public function __construct(array $uri)
    {
        parent::__construct($uri);
    }

    /**
     * @param null
     */
    public function getContent()
    {
        // print_r($GLOBALS);
        $arr = array(
            'uri' => $this->routeURI,
            'a' => 1,
            'b' => 2,
            'c' => 3,
            'd' => 4,
            'e' => 5
        );

        echo json_encode($arr);
    }

}
