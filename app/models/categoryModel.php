<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;


class categoryModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $db;
    public $blind;
    public $condition;
    public $elements;
    public $categoryInfo;
    public $result;

    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        
        $this->blind = array();
        $this->condition = array();
        $this->categoryInfo = array();
        $this->result = array();
    }

    /**
     * @param string $uri
     * @return array
     */
    public function getCategories(bool $count = false, int $id = null)
    {
        if (!self::isEmpty($id)) {
            $this->condition = array(
                'AND' => array(
                    'id = :categoryId',
                    'publish = 1',
                    'status = 1'
                )
            );
    
            $this->blind = array(':categoryId' => $id);

            $this->elements = array(
                'action'    => 'SELECT',
                'table'     => 'categories',
                'data'      => array (),
                'bind'      => $this->blind,
                'condition' => $this->condition,
                'range'     => 'single'
            );
    
            $this->result = $this->db->getDBAction($this->elements);
        }

        if (self::isEmpty($id)) {
            $this->query = "SELECT * FROM categories
                            WHERE publish = 1 AND status = 1";
    
            $this->db->getQuery($this->query);
            $this->categoryInfo = $this->db->getExecuteFetchAll(array());
    
            if ($count) {
                $this->result = array_merge(array('count' => count($this->categoryInfo)+0), $this->categoryInfo);
            } else {
                $this->result = $this->categoryInfo;
            }
            
        }

        if (!$this->result) {
            // Set a 400 (bad request) response code and exit.
            $this->result = array('error' => true, 'code' => 404);
        }

        return $this->result;
    }

    public function addCategory()
    {

    }

    public function updateCategory()
    {

    }

    public function deleteCategory()
    {

    }

}
