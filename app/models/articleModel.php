<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;


class articleModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $db;
    public $condition;
    public $actionElement;
    public $articleInfo;
    public $article;

    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->articleInfo = array();
        $this->article = array();
    }

    /**
     * @param string $uri
     * @return array
     */
    public function getArticle($articleId)
    {
        $this->condition = array(
            'AND' => array(
                'external = :external',
                'publish = 1'
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'opensef',
            'data'      => array (),
            'bind'      => array(':external' => '/' . $articleId),
            'condition' => $this->condition,
            'range'     => 'single'
        );

        $this->articleInfo = $this->db->getDBAction($this->actionElement);

        if ($this->articleInfo) {
            // 'access_level'
            // 'group_level'
            $this->condition = array(
                'AND' => array(
                    'id = :articleId',
                    'publish = 1',
                    'status = 1'
                )
            );

            $this->actionElement = array(
                'action'    => 'select',
                'table'     => $this->articleInfo['tbname'],
                'data'      => array (),
                'bind'      => array(':articleId' => $this->articleInfo['tid']),
                'condition' => $this->condition,
                'range'     => 'single'
            );

            $this->article = $this->db->getDBAction($this->actionElement);
        } else {
            // Set a 400 (bad request) response code and exit.
            $this->article = array('error' => true, 'code' => 404);
        }

        return $this->article;
    }
}
