<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;


class baseModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    /**
     * @var getWriteLog
     */
    private $log;

    /**
     * @var
     */
    public $db;

    public $query;

    /**
     * baseModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
    }

    /**
     * @param null $type
     * @return array
     */
    public function getMenu($type = null)
    {
        $menu = array();
        $this->query = "SELECT * FROM menu
                  WHERE menutype = :type AND parent = :parent AND publish = '1' AND status = '1' AND hideshow = '1'
                  ORDER BY ordering";
        $this->db->getQuery($this->query);
        $mainChannel = $this->db->getExecuteFetchAll(array(':type' => $type, ':parent' => 0));
        foreach ($mainChannel as $value) {
            $subMenu = array();
            $this->db->getQuery($this->query);
            $subChannel = $this->db->getExecuteFetchAll(array(':type' => $type, ':parent' => $value['id']));
            if (count($subChannel) > 0 ) {
                foreach ($subChannel as $subValue) {
                    $subMenu[] = [
                        'title' => $subValue['mtitle'],
                        'shortDescription' => $subValue['short_description'],
                        'subClass' => $subValue['subclass'],
                        'link' => self::isEmpty($subValue['link']) ? $subValue['alias'] : $subValue['link'],
                        'targetWindow' => $subValue['target_window'],
                    ];
                }
            }

            $menu[] = [
                'title' => $value['mtitle'],
                'shortDescription' => $value['short_description'],
                'subClass' => $value['subclass'],
                'link' => self::isEmpty($value['link']) ? $value['alias'] : $value['link'],
                'targetWindow' => $value['target_window'],
                'subMenu' => $subMenu
            ];
        }

        return $menu;
    }

    /**
     * @param string $uri
     * @return mixed
     */
    public function getMenuInfo(string $uri)
    {
        $this->query = "SELECT mtitle, alias, link FROM menu
                  WHERE alias = :alias AND publish = '1' AND status = '1' AND hideshow = '1'";
        $this->db->getQuery($this->query);
        $menuInfo = $this->db->getExecuteFetchAll(array(':alias' => $uri));

        return $menuInfo;
    }

    /**
     * @param string $code
     * @return mixed
     */
    public function getLanguageInfo($code)
    {
        $this->query = "SELECT * FROM languages WHERE active = '1' AND code = :code";
        $this->db->getQuery($this->query);
        $languageInfo = $this->db->getExecuteFetch(array(':code' => self::stringToLower($code)));

        return $languageInfo;
    }

}
