<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models\database;

use PDO;

/**
 * Class for Database connection.
 */
class getDB extends getConnect
{
    use \redBerg\Libs\libraries\getHandleElements;

    private static $log;
    private static $connect;
    public static $stmt;

    /**
     * getDB constructor.
     */
    public function __construct()
    {
        getDB::$log = new \redBerg\Libs\libraries\getWriteLog();
        getDB::$connect = $this->getOpen();

        try {
            // Check database has table or not
            $query = "SHOW TABLES";
            getDB::getQuery($query);
            $result = getDB::getExecuteFetchAll(array());

            if (!$result) {
                throw new \Exception("No db tables yet.");
            }
        } catch (\Exception $exception) {
            header('HTTP/1.1 503 Service Unavailable - ' . $exception->getMessage());
            getDB::$log->writeLog('Error', 'geDB: '. $exception->getMessage());

            die('<h1>No Service available</h1>' . $exception->getMessage());
        }
    }

    /**
     * @param $db_query
     */
    public static function getQuery($db_query)
    {
        try {
            //it takes away the threat of SQL Injection
            getDB::$stmt = getDB::$connect->prepare($db_query);
            if (!getDB::$stmt) {
                throw new \PDOException(getDB::$connect->errorInfo());
            }
        } catch (\PDOException $exception) {
            header('HTTP/1.1 500 Internal Server Error - ' . $exception->getMessage());
            getDB::$log->writeLog('Error', '1 geDB: '. $exception->getMessage());
            
            // echo '1. geDB: '. $exception->getMessage();
        }        
    }

    /**
     * Bind the inputs with the placeholders
     * $param placeholder like :name
     *
     * @param $param
     * @param $value
     */
    public static function getBind($param, $value)
    {
        if (is_int($value)) {
            $type = PDO::PARAM_INT;
        } elseif (is_bool($value)) {
            $type = PDO::PARAM_BOOL;
        } elseif (is_null($value)) {
            $type = PDO::PARAM_NULL;
        } else {
            $type = PDO::PARAM_STR;
        }

        // getDB::$stmt->bindParam(':'.$param, $value);
        getDB::$stmt->bindValue(':'.$param, $value, $type);
    }

    public static function getDBAction($action)
    {
        reset($action['data']);
        switch (self::stringToLower($action['action'])) {
            case 'insert':
                $query = 'INSERT INTO ' . $action['table'] . ' ( ';

                foreach ($action['data'] as $columns => $v) {
                    $query .= $columns . ', ';
                }

                $query = substr( $query, 0, -2 ) . ' ) VALUES ( ';
                reset($action['data']);

                foreach ($action['data'] as $c => $value) {
                    switch ((string)$value) {
                        case 'now()':
                            $query .= 'now(), ';
                            break;
                        case '':
                        case 'null':
                            $query .= 'null, ';
                            break;
                        default:
                            $query .= '\''. $value .'\', ';
                            break;
                    }
                }
                $query = substr( $query, 0, -2 ) . ' )';
                break;
            case 'update':
                $query = 'UPDATE ' . $action['table'] . ' SET ';

                foreach ($action['data'] as $columns => $value) {
                    switch ((string)$value) {
                        case 'now()':
                            $query .= $columns . ' = now(), ';
                            break;
                        case '':
                        case 'null':
                            $query .= $columns .= ' = null, ';
                            break;
                        default:
                            $query .= $columns . ' = \'' . $value . '\', ';
                            break;
                    }
                }

                $query = substr( $query, 0, -2 ) . ' WHERE ' . getDB::conditionMaker($action['condition']);
                break;
            default:
            case 'select':
                if (count($action['data']) == 0) {
                    $query = 'SELECT * ';
                } else {
                    $query = 'SELECT ';
                    foreach ($action['data'] as $key => $value) {
                        $query .= $value . ', ';
                    }
                }

                $query = substr($query, 0, -1) . ' FROM ' . $action['table'];

                if(!empty( $action['condition'])){
                    $query = substr($query, 0) . ' WHERE ' . getDB::conditionMaker($action['condition']);
                }
                break;
        }

        getDB::getQuery($query);

        switch ($action['range']) {
            case 'single':
                $result = getDB::getExecuteFetch($action['bind']);
                break;
            case 'all':
                $result = getDB::getExecuteFetchAll($action['bind']);
                break;
            default:
                $result = getDB::getExecute($action['bind']);
                break;
        }

        return $result;
    }

    /**
     *
     */
    public static function getExecute($bindValue)
    {
        try {
            getDB::$stmt->execute($bindValue);
            if (getDB::$stmt->errorCode() != 0) {
                throw new \PDOException(getDB::$stmt->errorInfo());
            }
        } catch (\PDOException $exception) {
            header('HTTP/1.1 500 Internal Server Error - ' . $exception->getMessage());
            getDB::$log->writeLog('Error', '2. geDB: '. $exception->getMessage());

            // echo '2. geDB: '. $exception->getMessage();
        }
    }

    /**
     * returns an array of the result set rows
     *
     * @param null $value
     * @return mixed
     */
    public static function getExecuteFetchAll($bindValue = null, $value = null)
    {
        try {
            getDB::getExecute($bindValue);

            $type = $value === 'OBJ' ? PDO::FETCH_OBJ : PDO::FETCH_ASSOC;
            $result = getDB::$stmt->fetchAll($type);

            // if (!$result) {
            //     throw new \PDOException('No Fetch All Result');
            // }

            getDB::$stmt->closeCursor();
    
            return $result;

        } catch (\PDOException $exception) {
            header('HTTP/1.1 500 Internal Server Error - ' . $exception->getMessage());
            getDB::$log->writeLog('Error', '3. geDB: '. $exception->getMessage());

            // echo '3. geDB: '. $exception->getMessage();
        }
    }

    /**
     * returns a single record from the database
     *
     * @param null $value
     * @return mixed
     */
    public static function getExecuteFetch($bindValue = null, $value = null)
    {
        try {
            getDB::getExecute($bindValue);

            $type = $value === 'OBJ' ? PDO::FETCH_OBJ : PDO::FETCH_ASSOC;
            $result = getDB::$stmt->fetch($type);

            // if (!$result) {
            //     throw new \PDOException('No Fetch result');
            // }

            getDB::$stmt->closeCursor();

            return $result;

        } catch (\PDOException $exception) {
            header('HTTP/1.1 500 Internal Server Error - ' . $exception->getMessage());
            getDB::$log->writeLog('Error', '4. geDB: '. $exception->getMessage());

            // echo '4. geDB: '. $exception->getMessage();
        }        
    }

    /**
     *  Returns number  of columns
     *
     *  @param  string $query
     *  @param  array  $params
     *  @return string
     */
    public static function getSingle($bindValue = null)
    {
        try {
            getDB::getExecute($bindValue);
            $result = getDB::$stmt->fetchColumn();

            // if (!$result) {
            //     throw new \PDOException('No Fetch Column result');
            // }

            // Frees up the connection to the server so that other SQL statements may be issued
            getDB::$stmt->closeCursor();

            return $result;

        } catch (\PDOException $exception) {
             header('HTTP/1.1 500 Internal Server Error - ' . $exception->getMessage());
            getDB::$log->writeLog('Error', '5. geDB: '. $exception->getMessage());

            // echo '5. geDB: '. $exception->getMessage();
        }        
    }

    /**
     * returns the number of effected rows from the previous delete, update or insert statement
     *
     * @return mixed
     */
    public static function getRowCount()
    {
        return getDB::$stmt->rowCount();
    }

    /**
     * returns the last inserted Id as a string
     *
     * @return mixed
     */
    public static function getLastInsertId()
    {
        return getDB::$connect->lastInsertId();
    }

    /**
     * Truncate table
     *
     * @param $table
     */
    public static function truncateTB($table)
    {
        exec("TRUNCATE TABLE $table");
    }

    /**
     *
     * @param $data array
     */
    public static function conditionMaker($data)
    {
        if (is_array($data)) {
            $idx = 0;
            $condition = '';
            foreach ($data as $idxA => $valueA) {
                foreach ($valueA as $idxB => $valueB) {
                    if ($idx > 0) {
                        $condition .= $idxA . ' ';
                    }

                    if (!is_numeric($idxB)) {
                        $subIdx = 0;
                        foreach($valueB as $idxC => $valueC) {
                            if ($subIdx == 0) {
                                $condition .= ' (';
                            }

                            if ($subIdx > 0) {
                                $condition .= $idxB . ' ';
                            }

                            $condition .= $valueC . ' ';

                            if (count($valueB) -1 == $subIdx) {
                                $condition .= ') ';
                            }

                            $subIdx ++;
                        }

                    } else {
                        $condition .= $valueB . ' ';
                    }

                    $idx++;
                }
            }
        } else {
            $condition = $data;
        }


        return $condition;
    }
}
