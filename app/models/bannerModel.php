<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;


class bannerModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    private $log;
    private $db;
    private $query;
    private $parameters;
    private $count;
    private $banners;
    private $allBanners;

    /**
     * boardModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
        $this->count = 0;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getCountAllBannerPerPage(int $pageId)
    {
        $this->parameters = array(
            ':pageId' => $pageId
        );

        $this->query = "SELECT id FROM banners
                        WHERE pageid LIKE CONCAT('%[', :pageId, ']%') AND publish = '1' AND status = '1'";

        $this->db->getQuery($this->query);
        $this->banners = $this->db->getExecuteFetchAll($this->parameters);

        $this->count = count($this->banners)+0;

        return $this->count;
    }

    public function getAllBannerPerPage(int $pageId)
    {
        $this->parameters = array(
            ':pageId' => $pageId
        );

        $this->query = "SELECT * FROM banners WHERE pageid LIKE CONCAT('%[', :pageId, ']%') AND publish = '1' AND status = '1' ORDER BY ordering";

        $this->db->getQuery($this->query);
        $this->allBanners = $this->db->getExecuteFetchAll($this->parameters);

        return $this->allBanners;
    }
}
