<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;

class homeModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    private $log;
    private $db;
    private $query;
    private $parameter;

    private $menu;
    private $page;
    private $pageView;
    private $publish;
    private $status;

    /**
     * pageModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
        $this->menu = array();
        $this->page = array();
        $this->parameter = array();
        $this->publish = 1;
        $this->status = 1;
    }

    /**
     * @return array
     */
    public function getHomePage()
    {
        $this->parameter = array(
            ':frontPage' => 1
        );

        $this->query = "SELECT * FROM pages WHERE frontpage = :frontPage AND publish = $this->publish AND status = $this->status";
        $this->db->getQuery($this->query);
        $this->page = $this->db->getExecuteFetch($this->parameter);

        $this->updateView();

        return $this->page;
    }

    public function updateView() 
    {
        $this->parameter = array(
            ':frontPage' => 1
        );

        $this->query = "UPDATE pages SET views = views + 1 WHERE frontpage = :frontPage AND publish = $this->publish AND status = $this->status";
        $this->db->getQuery($this->query);
        $this->pageView = $this->db->getExecuteFetch($this->parameter);
    }
}
