<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;


class sectionModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $db;
    public $blind;
    public $condition;
    public $elements;
    public $sectionInfo;
    public $result;

    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        
        $this->blind = array();
        $this->condition = array();
        $this->sectionInfo = array();
        $this->result = array();
    }

    /**
     * @param string $uri
     * @return array
     */
    public function getSections(int $id = null)
    {
        if (!self::isEmpty($id)) {
            $this->condition = array(
                'AND' => array(
                    'id = :sectionId',
                    'publish = 1',
                    'status = 1'
                )
            );
    
            $this->blind = array(':sectionId' => $id);

            $this->elements = array(
                'action'    => 'SELECT',
                'table'     => 'sections',
                'data'      => array (),
                'bind'      => $this->blind,
                'condition' => $this->condition,
                'range'     => 'single'
            );
    
            $this->result = $this->db->getDBAction($this->elements);
        }

        if (self::isEmpty($id)) {
            $this->query = "SELECT * FROM sections
                            WHERE publish = 1 AND status = 1";
    
            $this->db->getQuery($this->query);
            $this->sectionInfo = $this->db->getExecuteFetchAll(array());
    
            $this->result = array_merge(array('count' => count($this->sectionInfo)+0), $this->sectionInfo );
        }

        if (!$this->result) {
            // Set a 400 (bad request) response code and exit.
            $this->result = array('error' => true, 'code' => 404);
        }

        return $this->result;
    }

    public function addSection()
    {

    }

    public function updateSection()
    {

    }

    public function deleteSection()
    {

    }

}
