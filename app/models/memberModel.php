<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;

class memberModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    private $log;
    private $db;
    private $query;
    private $parameter;

    private $member;
    private $publish;
    private $status;

    /**
     * pageModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
        $this->parameter = array();
        $this->publish = 1;
        $this->status = 1;
    }

    public function getUser($userId)
    {
        $this->condition = array(
            'AND' => array(
                'id = :userId',
                'publish = \'1\'',
                'members_status = \'1\''
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(':userId' =>   $userId),
            'condition' => $this->condition,
            'range'     => 'single'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array('error' => true, 'code' => 404);
        }

        return $this->userInfo;
    }

    public function getMemberByEmail(string $email)
    {
        $this->parameter = array(
            ':email' => $email
        );

        $this->query = "SELECT * FROM members WHERE members_email = :email AND publish = $this->publish AND members_status = $this->status";
        $this->db->getQuery($this->query);
        $this->member = $this->db->getExecuteFetchAll($this->parameter);

        return $this->member;
    }

    public function getMemberById(int $id)
    {
        $this->parameter = array(
            ':id' => $id
        );

        $this->query = "SELECT * FROM members WHERE id = :id AND publish = $this->publish AND members_status = $this->status";
        $this->db->getQuery($this->query);
        $this->member = $this->db->getExecuteFetchAll($this->parameter);

        return $this->member;
    }


    public function addNewMember(int $id, array $conditions)
    {
        // $conditions = array('member_firstname' => '', 'member_alias' => '');

        $this->condition = array(
            'AND' => array(
                'alias = :alias',
                'publish = 1',
                'status = 1',
                'hideshow = 1',
                'pid != 0'
            )
        );

        $this->actionElement = array(
            'action'    => 'update',
            'table'     => 'menu',
            'data'      => array (),
            'bind'      => array(':alias' => '/' . $uri['uri']),
            'condition' => $this->condition,
            'range'     => 'single'
        );
        
        $this->menu = $this->db->getDBAction($this->actionElement);
    }

    public function updateMember(int $id)
    {

    }

    public function removeMember(int $id ) 
    {

    }
}
