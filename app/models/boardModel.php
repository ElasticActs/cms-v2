<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;

class boardModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $log;
    public $db;
    public $query;
    public $status;
    public $section;
    public $category;
    public $boardList;
    public $boardItem;

    /**
     * boardModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
    }


    public function getCheckSectionCategory(int $sid, int $cid)
    {
        $this->query = "SELECT count(*) as NUM 
                        FROM categories
                        WHERE id = :categoryId AND section = :sectionId AND publish = 1 AND status = 1";
        $this->db->getQuery($this->query);
        $this->status = $this->db->getExecuteFetch(array(':categoryId' => $cid, ':sectionId' => $sid));

        if ($this->status['NUM'] == 1) {
            $this->status = true;
        } else {
            $this->status = false;
        }

        return $this->status;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getSectionInfo(int $id) : array
    {
        $this->query = "SELECT * 
                        FROM sections
                        WHERE id = :id AND publish = 1 AND status = 1";
        $this->db->getQuery($this->query);
        $this->section = $this->db->getExecuteFetch(array(':id' => $id));

        return $this->section;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getCategoryInfo(int $id) : array
    {
        $this->query = "SELECT * 
                        FROM categories
                        WHERE id = :id AND publish = 1 AND status = 1";
        $this->db->getQuery($this->query);
        $this->category = $this->db->getExecuteFetch(array(':id' => $id));

        return $this->category;
    }

    public function getBoardList(int $sectionId, int $categoryId, int $start = null, int $end = null) : array
    {
        $first = $start == null ? 0 : $start;
        $max = $end == null ? 100 : $end;
        $this->query = "SELECT 
                        b.id, b.title, b.title_alias, b.summarytxt, 
                        b.filename, b.host, b.casting_description, b.embed_bigcode, b.casting_date, b.views, 
                        b.votes, b.publish_date, o.external, o.pid, m.alias as pagelink
                        FROM articles b, opensef  o
                        LEFT JOIN menu m ON o.pid = m.pid
                        WHERE b.id = o.tid
                            AND o.tbname = 'articles'
                            AND b.notice = 0
                            AND o.publish = 1
                            AND m.status = 1
                            AND b.access_level >= ''
                            AND b.group_level >= ''
                            AND b.publish = 1
                            AND b.status = 1
                            AND sectionid = :sectionId AND categoriesid = :categoryId
                        ORDER BY b.ordering DESC";
        $this->db->getQuery($this->query);
        $this->boardList = $this->db->getExecuteFetchAll(array(
                                                ':sectionId' => $sectionId,
                                                ':categoryId' => $categoryId
                                                ));

        return $this->boardList;
    }

    // public function getBoardItem(int $sectionId, int $categoryId) : array
    // {
    //     $first = 0;
    //     $max = 100 ;
    //     $this->query = "SELECT
    //                     b.id, b.title, b.title_alias, b.thumbnail, b.summarytxt,
    //                     b.qrcode, b.fulltxt, b.linkfile, b.filename, b.filesize, b.urls, b.host, b.casting_description,
    //                     b.casting_date, b.embed_bigcode, b.embed_code, b.sectionid, b.categoriesid, b.ordering, b.metatitle,
    //                     b.metakey, b.metadesc, b.access_level, b.publish, b.status, b.notice, b.linkOpt, b.loginDownload,
    //                     b.downloadAlias, b.views, b.votes, b.password, b.user_ip, b.modified_date, b.modified_by,
    //                     b.created_date, b.created_by, b.created_by_alias, b.publish_date, o.external, o.pid, m.alias as pagelink
    //                     FROM articles b, opensef  o
    //                     LEFT JOIN menu m ON o.pid = m.pid
    //                     WHERE b.id = o.tid
    //                         AND o.tbname = 'articles'
    //                         AND b.notice = 0
    //                         AND o.publish = 1
    //                         AND m.status = 1
    //                         AND b.access_level >= ''
    //                         AND b.group_level >= ''
    //                         AND b.publish = 1
    //                         AND b.status = 1
    //                         AND sectionid = :sectionId AND categoriesid = :categoryId
    //                     ORDER BY b.ordering DESC LIMIT {$first}, {$max}";
    //     $this->db->getQuery($this->query);
    //     $this->boardItem = $this->db->getExecuteFetchAll(array(
    //                                             ':sectionId' => $sectionId,
    //                                             ':categoryId' => $categoryId,
    //                                             // ':limitFirst' => $first,
    //                                             // ':limitLast' => $max
    //                                             ));

    //     return $this->boardItem;
    // }
}
