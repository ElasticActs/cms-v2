<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;

class moduleModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $log;
    public $db;
    public $query;
    public $parameters;
    public $count;
    public $modules;
    public $allModules;

    /**
     * boardModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
        $this->count = 0;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getCountAllModulesPerPage(int $pageId)
    {
        $this->parameters = array(
            ':pageId' => $pageId
        );

        $this->query = "SELECT app_id
                  FROM apps_pages ap
                  LEFT JOIN modules m ON m.id = ap.app_id
                  WHERE ap.pageid LIKE CONCAT('%[', :pageId, ']%') AND m.publish = '1' AND m.status = '1' AND ap.publish = '1'";

        $this->db->getQuery($this->query);
        $this->modules = $this->db->getExecuteFetchAll($this->parameters);

        $this->count = count($this->modules)+0;

        return $this->count;
    }

    public function getAllModulesPerPage(int $pageId)
    {
        $this->parameters = array(
            ':pageId' => $pageId
        );

        // need position, showtitle in the db table?
        $this->query = "SELECT app_id as moduleToPageId, app_position, ap.ordering, m.id AS moduleId, title, fulltxt, showtitle, filename, inpage AS insidePage
                  FROM apps_pages ap
                  LEFT JOIN modules m ON m.id = ap.app_id
                  WHERE ap.pageid LIKE CONCAT('%[', :pageId, ']%') AND m.publish = '1' AND m.status = '1' AND ap.publish = '1'
                  ORDER BY m.ordering DESC";
        $this->db->getQuery($this->query);
        $this->allModules = $this->db->getExecuteFetchAll($this->parameters);

        return $this->allModules;
    }

}
