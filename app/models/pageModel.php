<?php
/**
 * @package  redBerg CMS
 * @author   dev@kenwoo.ca
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons
 */
namespace redBerg\App\models;

class pageModel
{
    use \redBerg\Libs\libraries\getHandleElements;

    public $log;
    public $db;
    public $condition;
    public $element;
    public $menu = array();
    public $return = array();

    /**
     * pageModel constructor.
     */
    public function __construct()
    {
        $this->db = new \redBerg\App\models\database\getDB();
        $this->log = new \redBerg\Libs\libraries\getWriteLog();
    }

    /**
     * @param string $uri
     * @return array
     */
    public function getPage(array $uri)
    {
        $this->condition = array(
            'AND' => array(
                'alias = :alias',
                'publish = 1',
                'status = 1',
                'hideshow = 1',
                'pid != 0'
            )
        );

        $this->element = array(
            'action'    => 'select',
            'table'     => 'menu',
            'data'      => array (),
            'bind'      => array(':alias' => '/' . $uri['uri']),
            'condition' => $this->condition,
            'range'     => 'single'
        );
        
        $this->menu = $this->db->getDBAction($this->element);

        if ($this->menu) {
            $this->condition = array(
                'AND' => array(
                    'id = :pageId',
                    'publish = \'1\'',
                    'status = \'1\''
                )
            );
            $this->element = array(
                'action'    => 'select',
                'table'     => 'pages',
                'data'      => array(),
                'bind'      => array(':pageId' => $this->menu['pid']),
                'condition' => $this->condition, // "id = :pageId AND publish = '1' AND status = '1'",
                'range'     => 'single'
            );
            $this->return = $this->db->getDBAction($this->element);
        } else {
            // Set a 400 (bad request) response code and exit.
            $this->return = array('error' => true, 'code' => 404);
        }

        return $this->return;
    }
}
